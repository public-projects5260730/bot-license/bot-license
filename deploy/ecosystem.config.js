module.exports = {
  apps: [
    {
      name: "Bot License",
      script: "./dist/apps/api/main.js",
      env: {
        PORT: null,
        GATEWAY_PORT: null,
        DB_MSQL_HOST: "",
        DB_MSQL_POST: "3306",
        DB_MSQL_USERNAME: "bot-license",
        DB_MSQL_PASSWORD: "",
        DB_MSQL_DATABASE: "bot-license",

        LOGGER_USE_CONSOLE: false,
        LOGGER_USE_FILE: false,
        LOGGER_USE_MONGO_DB: "",
        LOGGER_USE_MONGO_DB_NAME: "bot-license-logger",

        GUARD_PASS_KEY: "",
      },
      exec_mode: "cluster",
    },
    {
      name: "back-end",
      script: "serve",
      env: {
        PM2_SERVE_PATH: "./dist/apps/back-end/main.js",
        PM2_SERVE_PORT: null,
        PM2_SERVE_SPA: "true",
      },
      exec_mode: "cluster",
    },
  ],
};
