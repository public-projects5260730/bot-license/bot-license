import { config } from 'dotenv';
config();

import { DataSource } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

const soruce = new DataSource({
  type: 'mysql',
  namingStrategy: new SnakeNamingStrategy(),
  host: process?.env?.DB_MSQL_HOST || 'localhost',
  port: +(process?.env?.DB_MSQL_POST || 3306),
  username: process?.env?.DB_MSQL_USERNAME || 'root',
  password: process?.env?.DB_MSQL_PASSWORD || '',
  database: process?.env?.DB_MSQL_DATABASE || 'demo',

  logging: false,

  migrations: ['./database/migrations/*.{ts,js}'],
  migrationsTableName: 'nest_migrations',
});

soruce.initialize().then(() => {
  console.log('connect db success');
});

export default soruce;
