import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Licenses1683683288497 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'licenses',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'agent_id',
            type: 'int',
            unsigned: true,
            default: 0,
          },
          {
            name: 'license_key',
            type: 'varchar',
            length: '50',
            isUnique: true,
          },
          {
            name: 'account_number',
            type: 'int',
          },
          {
            name: 'license_days',
            type: 'int',
          },
          {
            name: 'license_options',
            type: 'json',
            isNullable: true,
          },
          {
            name: 'state',
            type: 'int',
            unsigned: true,
            default: 1,
            comment: '1: new, 2: activated, 3: expired, 4: canceled, 5: revoked',
          },
          {
            name: 'reason',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'extra',
            type: 'json',
            isNullable: true,
          },
          {
            name: 'activated_at',
            type: 'timestamp',
            isNullable: true,
          },
          {
            name: 'expired_at',
            type: 'timestamp',
            isNullable: true,
          },
          {
            name: 'is_trial',
            type: 'tinyint',
            length: '1',
            default: 0,
          },
          {
            name: 'is_synced',
            type: 'tinyint',
            length: '1',
            default: 0,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            onUpdate: 'CURRENT_TIMESTAMP',
          },
        ],
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('licenses');
  }
}
