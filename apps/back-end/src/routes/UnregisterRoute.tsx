import { Navigate, Outlet } from 'react-router-dom';
import { useAppSelector } from '@back-end/hooks';
import { selectVisitorId } from '@back-end/store/auth';

const UnregisterRoute = () => {
  const visitorId = useAppSelector(selectVisitorId);
  return !visitorId ? <Outlet /> : <Navigate to="/" />;
};
export default UnregisterRoute;
