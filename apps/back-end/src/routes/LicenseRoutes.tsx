import { LicenseListPage } from '@back-end/features/licenses';
import { LicenseViewPage } from '@back-end/features/licenses/view';
import { Route, Routes } from 'react-router-dom';

const LicenseRoutes = () => {
  return (
    <Routes>
      <Route index path="*" element={<LicenseListPage />} />
      <Route path=":id/view" element={<LicenseViewPage />} />
    </Routes>
  );
};

export default LicenseRoutes;
