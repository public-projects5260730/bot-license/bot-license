import { AgentListPage } from '@back-end/features/agents';
import { Route, Routes } from 'react-router-dom';

const AgentRoutes = () => {
  return (
    <Routes>
      <Route index path="*" element={<AgentListPage />} />
    </Routes>
  );
};

export default AgentRoutes;
