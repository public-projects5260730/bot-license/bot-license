import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { LazyLoad } from '@react-libs/utils';

const RouteComponent = () => {
  const Layout = LazyLoad(() => import('@back-end/layouts'));
  const RegisteredRoute = LazyLoad(() => import('./RegisteredRoute'));
  const UnregisterRoute = LazyLoad(() => import('./UnregisterRoute'));
  const AgentRoutes = LazyLoad(() => import('./AgentRoutes'));
  const LicenseRoutes = LazyLoad(() => import('./LicenseRoutes'));

  const HomePage = LazyLoad(() => import('@back-end/features/home'));
  const LoginPage = LazyLoad(() => import('@back-end/features/auth/login'));
  const LogoutPage = LazyLoad(() => import('@back-end/features/auth/logout'));

  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Layout />}>
          <Route element={<RegisteredRoute />}>
            <Route index path="*" element={<HomePage />} />

            <Route path="agents/*" element={<AgentRoutes />} />
            <Route path="licenses/*" element={<LicenseRoutes />} />

            <Route path="logout" element={<LogoutPage />} />
          </Route>
          <Route element={<UnregisterRoute />}>
            <Route path="login" element={<LoginPage />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
export default RouteComponent;
