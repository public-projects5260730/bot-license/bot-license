import { LicenseDtos } from '@dtos';
import React from 'react';

const LicenseViewContext = React.createContext<{
  license?: LicenseDtos.LicenseDto;
  isLoading?: boolean;
}>({});

export default LicenseViewContext;
