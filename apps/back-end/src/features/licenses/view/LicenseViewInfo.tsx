import { useHeader, useTrans } from '@back-end/hooks';
import { useContext } from 'react';
import LicenseViewContext from './LicenseView.context';
import { LicenseListActions } from '../list';
import Form from '@react-libs/components/Form';
import { formatNumber } from '@utils';
import { LicenseStateBadge, LicenseTrialBadge } from '../common';
import { formatDate } from '@react-libs/utils';
import { tooltip } from '@back-end/utils';
import { toast } from 'react-toastify';

export const LicenseViewInfo = () => {
  const trans = useTrans();

  const { license } = useContext(LicenseViewContext);

  useHeader({
    pageTitle: `${trans('license_view')}: ${license?.licenseKey}`,
    section: 'licenses',
    breadcrumbs: [
      {
        title: trans('licenses'),
        link: '/licenses',
        icon: 'fas fa-handshake',
      },

      {
        title: trans('view'),
        active: true,
      },
    ],
  });

  if (!license?.id) {
    return <></>;
  }

  return (
    <div className="card card-info">
      <div className="card-header">
        <div className="card-title text-capitalize">
          {trans('information')}: <span className="badge badge-dark">{license.licenseKey}</span>
        </div>
        <div className="card-tools">{!!license.id && <LicenseListActions license={license} />}</div>
      </div>
      <div className="card-body">
        <Form.Row label={trans('license_key')}>
          <div className="input-group">
            <div className="form-control fake-form-control disabled text-truncate">{license.licenseKey}</div>
            <div className="input-group-append">
              <span className="input-group-text">
                <i
                  className="fas fa-copy cursor-pointer"
                  {...tooltip(trans('copied'))}
                  onClick={() => {
                    navigator.clipboard.writeText(license.licenseKey);
                    toast.dismiss();
                    toast.success(trans('copied'));
                  }}
                ></i>
              </span>
            </div>
          </div>
        </Form.Row>

        <Form.Row label={trans('agent')}>
          <div className="form-control fake-form-control disabled">{license.agent?.name || 'N/A'}</div>
        </Form.Row>

        <Form.Row label={trans('account_number')}>
          <div className="form-control fake-form-control disabled">
            {license.accountNumber >= 0
              ? `${formatNumber(license.activatedAccountIds?.length)} / ${formatNumber(license.accountNumber)}`
              : trans('unlimited')}
          </div>
        </Form.Row>

        <Form.Row label={trans('license_days')}>
          <div className="form-control fake-form-control disabled">
            {license.licenseDays >= 0 ? `${formatNumber(license.licenseDays)} ${trans('days')}` : trans('unlimited')}
          </div>
        </Form.Row>

        <Form.Row label={trans('type')}>
          <div className="form-control fake-form-control disabled">
            <LicenseTrialBadge license={license} />
          </div>
        </Form.Row>

        <Form.Row label={trans('state')}>
          <div className="form-control fake-form-control disabled">
            <LicenseStateBadge license={license} />
          </div>
        </Form.Row>

        <Form.Row label={trans('reason')}>
          <div className="form-control fake-form-control disabled">{license.reason}</div>
        </Form.Row>

        <Form.Row label={trans('activated_at')}>
          <div className="form-control fake-form-control disabled">
            {license?.activatedAt ? formatDate(license.activatedAt) : 'N/A'}
          </div>
        </Form.Row>

        <Form.Row label={trans('expired_at')}>
          <div className="form-control fake-form-control disabled">
            {license?.expiredAt ? formatDate(license.expiredAt) : 'N/A'}
          </div>
        </Form.Row>

        <Form.Row label={trans('created_at')}>
          <div className="form-control fake-form-control disabled">{formatDate(license.createdAt)}</div>
        </Form.Row>
      </div>
    </div>
  );
};
