import { useTrans } from '@back-end/hooks';
import { CardError } from '@react-libs/components/Common';
import { useParams } from 'react-router-dom';
import { LicenseViewContainer } from './LicenseView.container';

export const LicenseViewPage = () => {
  const trans = useTrans();
  const { id } = useParams<{ id?: string }>();

  if (!id || isNaN(+id)) {
    return <CardError error={trans('miss_id')} />;
  }

  return <LicenseViewContainer id={+id} />;
};
