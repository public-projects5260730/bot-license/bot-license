import { useTrans } from '@back-end/hooks';
import { useContext } from 'react';
import LicenseViewContext from './LicenseView.context';
import { formatDate } from '@react-libs/utils';

export const LicenseViewAccounts = () => {
  const trans = useTrans();

  const { license } = useContext(LicenseViewContext);

  if (!license?.activatedLicenses?.length) {
    return <></>;
  }

  return (
    <div className="card card-info">
      <div className="card-header">
        <div className="card-title text-capitalize">{trans('activated_accounts')}</div>
      </div>
      <div className="card-body p-0">
        <ul className="list-group list-group-flush grid-repeat-fill-25x">
          {license.activatedLicenses.map(({ accountId, createdAt }) => (
            <li className="list-group-item list-group-item-action" key={accountId}>
              <b>{accountId}</b>
              <div className="text-muted small">{formatDate(createdAt)}</div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};
