import { useTrans } from '@back-end/hooks';
import { useContext } from 'react';
import LicenseViewContext from './LicenseView.context';

export const LicenseViewOption = () => {
  const trans = useTrans();

  const { license } = useContext(LicenseViewContext);

  return (
    <div className="grid-repeat-fit-28x">
      <div className="card card-info">
        <div className="card-header">
          <div className="card-title text-capitalize">{trans('license_options')}</div>
        </div>
        <div className="card-body p-0">
          <div className="form-control disabled h-auto">
            <pre>{JSON.stringify(license?.licenseOptions, null, 4)}</pre>
          </div>
        </div>
      </div>
      <div className="card card-info">
        <div className="card-header">
          <div className="card-title text-capitalize">{trans('bot_options')}</div>
        </div>
        <div className="card-body p-0">
          <div className="form-control disabled h-auto">
            <pre>{JSON.stringify(license?.botOptions, null, 4)}</pre>
          </div>
        </div>
      </div>
    </div>
  );
};
