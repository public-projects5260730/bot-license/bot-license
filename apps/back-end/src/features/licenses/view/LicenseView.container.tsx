import { licenseApi } from '@back-end/apis';
import { LicenseDtos } from '@dtos';
import { useEffect, useState } from 'react';
import { from } from 'rxjs';
import LicenseViewContext from './LicenseView.context';
import { CardNoData, LoadingOverlay } from '@react-libs/components/Common';
import { LicenseViewInfo } from './LicenseViewInfo';
import { LicenseViewOption } from './LicenseViewOption';
import { LicenseViewAccounts } from './LicenseViewAccounts';

export const LicenseViewContainer = ({ id }: { id: number }) => {
  const [license, setLicense] = useState<LicenseDtos.LicenseDto>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);

    const subscription = from(licenseApi.fetchView(id)).subscribe(({ license }) => {
      setIsLoading(false);
      setLicense(license);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [id]);

  if (isLoading) {
    return <LoadingOverlay />;
  }

  if (!license?.id) {
    return <CardNoData />;
  }

  return (
    <LicenseViewContext.Provider value={{ license, isLoading }}>
      <div className="row">
        <div className="col-md-5">
          <LicenseViewInfo />
        </div>
        <div className="col-md-7">
          <LicenseViewOption />
          <LicenseViewAccounts />
        </div>
      </div>
    </LicenseViewContext.Provider>
  );
};
