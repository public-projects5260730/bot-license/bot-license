import { useTrans } from '@back-end/hooks';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { LicenseEditForm } from './LicenseEdit.form';
import { LicenseDtos } from '@dtos';

export const LicenseEditModal = ({ license, className }: { license: LicenseDtos.LicenseDto; className?: string }) => {
  const trans = useTrans();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button className={`btn btn-danger btn-sm ${className}`} onClick={() => setShowModal(!showModal)}>
        <i className="fa fa-edit mr-2"></i>
        {trans('edit')}
      </button>

      <Modal
        show={showModal}
        onHide={() => setShowModal(!showModal)}
        aria-labelledby="licenses-edit"
        size="lg"
        centered
        animation={false}
      >
        <Modal.Header closeButton className="bg-warning">
          <Modal.Title id="licenses-edit" className="text-truncate">
            {trans('edit_license')}: {license.licenseKey}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LicenseEditForm license={license} />
        </Modal.Body>
      </Modal>
    </>
  );
};
