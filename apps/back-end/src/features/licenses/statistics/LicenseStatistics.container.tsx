import { licenseApi } from '@back-end/apis';
import { useTrans } from '@back-end/hooks';
import { LicenseDtos } from '@dtos';
import { useEffect, useState } from 'react';
import { from } from 'rxjs';
import { LICENSE_STATES, formatNumber } from '@utils';
import { enumLabel } from '@react-libs/utils';
import { Link } from 'react-router-dom';
import { licenseStateClass } from '@back-end/utils';

export const LicenseStatisticsContainer = () => {
  const trans = useTrans();
  const [statistics, setStatistics] = useState<LicenseDtos.LicenseStatisticsDto[]>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);

    const subscription = from(licenseApi.fetchStatistics()).subscribe(({ statistics }) => {
      setIsLoading(false);
      setStatistics(statistics || []);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  if (!statistics?.length) {
    return <></>;
  }

  return (
    <div className="card card-info card-outline">
      <div className="card-header">
        <div className="card-title text-capitalize">{trans('license_statistics')}</div>
      </div>
      <div className="card-body">
        <div className="grid-repeat-fit-20x">
          {statistics.map(({ state, counter }) => (
            <div className={`small-box ${licenseStateClass(state)}`}>
              <div className="inner">
                <div className="h3">{formatNumber(counter)}</div>
                <p>{trans(enumLabel(LICENSE_STATES, state, 'licenses_states'))}</p>
              </div>
              <div className="icon">
                <i className="ion fas fa-flag"></i>
              </div>
              <Link
                to={{
                  pathname: '/licenses',
                  search: new URLSearchParams({ state: `${state}` }).toString(),
                }}
                className="small-box-footer"
              >
                {trans('more_info')}
                <i className="fas fa-arrow-circle-right ml-2"></i>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
