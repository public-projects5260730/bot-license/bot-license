import { useTrans } from '@back-end/hooks';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { LicenseRevokedForm } from './LicenseRevoked.form';
import { LicenseDtos } from '@dtos';
import { LICENSE_STATES } from '@utils';

export const LicenseRevokedModal = ({
  license,
  className,
}: {
  license: LicenseDtos.LicenseDto;
  className?: string;
}) => {
  const trans = useTrans();

  const [showModal, setShowModal] = useState(false);

  if (![LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED].includes(license.state)) {
    return <></>;
  }

  return (
    <>
      <button className={`btn btn-danger btn-sm ${className}`} onClick={() => setShowModal(!showModal)}>
        <i className="fas fas fa-ban mr-2"></i>
        {trans('revoked')}
      </button>
      <Modal
        show={showModal}
        onHide={() => setShowModal(!showModal)}
        aria-labelledby="licenses-revoked"
        centered
        animation={false}
      >
        <Modal.Header closeButton className="bg-danger">
          <Modal.Title id="licenses-revoked" className="text-truncate">
            {trans('revoked_license')}: {license.licenseKey}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LicenseRevokedForm license={license} />
        </Modal.Body>
      </Modal>
    </>
  );
};
