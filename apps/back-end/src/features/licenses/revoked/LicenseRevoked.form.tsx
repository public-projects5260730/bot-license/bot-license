import { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useReloadPage, useTrans } from '@back-end/hooks';
import { licenseApi } from '@back-end/apis';
import Form from '@back-end/components/Form';
import { LicenseDtos } from '@dtos';
import { IEditLicenseFormData } from '@back-end/types';
import { LICENSE_STATES } from '@utils';

export const LicenseRevokedForm = ({ license }: { license: LicenseDtos.LicenseDto }) => {
  const trans = useTrans();
  const reloadPage = useReloadPage();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [formError, setFormError] = useState<string>();

  const methods = useForm<IEditLicenseFormData>();
  const { handleSubmit, setError } = methods;

  const onSubmit = handleSubmit((formData) => {
    if (isSubmitting) {
      return;
    }
    setFormError('');
    setIsSubmitting(true);

    licenseApi
      .sendEdit(license.id, { ...formData, state: LICENSE_STATES.REVOKED })
      .then(({ license, errors, error }) => {
        setIsSubmitting(false);
        if (error) {
          setFormError(error);
        }
        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof IEditLicenseFormData, {
              type: 'manual',
              message,
            });
          });
        }

        if (license?.id) {
          reloadPage('/licenses');
        }
      });
  });

  return (
    <FormProvider {...methods}>
      <form method="GET" onSubmit={onSubmit}>
        {!!formError?.length && <div className="callout callout-danger text-danger p-2 small">{formError}</div>}

        <Form.Row name="reason">
          <Form.InputGroup prepend={<i className="fas fa-sticky-note"></i>}>
            <Form.TextAreaBox name="reason" rows={3} />
          </Form.InputGroup>
        </Form.Row>

        <hr />
        <div className="text-right">
          <Form.Button
            className="btn btn-danger"
            isSubmitting={isSubmitting}
            label={trans('revoked')}
            icon={<i className="fas fas fa-ban mr-2"></i>}
          />
        </div>
      </form>
    </FormProvider>
  );
};
