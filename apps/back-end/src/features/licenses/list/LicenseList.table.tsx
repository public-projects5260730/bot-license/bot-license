import { useTrans } from '@back-end/hooks';
import { useContext } from 'react';
import LicenseListContext from './LicenseList.context';
import { formatNumber } from '@utils';
import { LicenseListActions } from './LicenseList.actions';
import { formatDate } from '@back-end/utils';
import { LicenseStateBadge, LicenseTrialBadge } from '../common';
import { RowNoData } from '@react-libs/components/Common';

export const LicenseListTable = () => {
  const trans = useTrans();
  const { paginator, error } = useContext(LicenseListContext);

  return (
    <div>
      {!!error?.length && <div className="callout callout-danger text-danger p-2 small">{error}</div>}
      <div className="table-responsive">
        <table className="table table-hover table-striped text-nowrap table-layout-fixed">
          <thead className="text-center">
            <tr>
              <th className="w-20 text-left">{trans('license_key')}</th>
              <th className="text-left">{trans('agent')}</th>
              <th>{trans('account_number')}</th>
              <th>{trans('license_days')}</th>
              <th>{trans('type')}</th>
              <th>{trans('state')}</th>
              <th>{trans('activated_at')}</th>
              <th>{trans('expired_at')}</th>
              <th>{trans('created_at')}</th>
              <th className="text-right">{trans('action')}</th>
            </tr>
          </thead>
          <tbody className="text-center">
            {!paginator?.items?.length && <RowNoData />}
            {paginator?.items?.map((item) => (
              <tr key={item.id}>
                <td className="text-left">{item.licenseKey}</td>
                <td className="text-left">{item?.agent?.name || 'N/A'}</td>
                <td>
                  {item.accountNumber >= 0
                    ? `${formatNumber(item?.activatedAccountIds?.length)} / ${formatNumber(item.accountNumber)}`
                    : trans('unlimited')}
                </td>
                <td>
                  {item.licenseDays >= 0 ? `${formatNumber(item.licenseDays)} ${trans('days')}` : trans('unlimited')}
                </td>
                <td>
                  <LicenseTrialBadge license={item} />
                </td>
                <td>
                  <LicenseStateBadge license={item} />
                </td>
                <td>{item?.activatedAt ? formatDate(item.activatedAt) : 'N/A'}</td>
                <td>{item?.expiredAt ? formatDate(item.expiredAt) : 'N/A'}</td>
                <td>{formatDate(item.createdAt)}</td>
                <td className="text-right">
                  <LicenseListActions license={item} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
