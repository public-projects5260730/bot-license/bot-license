import { licenseApi } from '@back-end/apis';
import { useFilters, useTrans } from '@back-end/hooks';
import { LicenseDtos, PaginatorDto } from '@dtos';
import { useEffect, useState } from 'react';
import { from } from 'rxjs';
import LicenseListContext from './LicenseList.context';
import { LicenseListFilter } from './LicenseList.filter';
import { LicenseListTable } from './LicenseList.table';
import { LoadingOverlay, Pagination } from '@react-libs/components/Common';
import { LicenseAddModal } from '../add';

export const LicenseListContainer = () => {
  const trans = useTrans();
  const [paginator, setPaginator] = useState<PaginatorDto<LicenseDtos.LicenseDto>>();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string>();

  const { filters } = useFilters();

  useEffect(() => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);

    const subscription = from(licenseApi.fetchPaginator(filters)).subscribe(({ paginate, error }) => {
      setIsLoading(false);
      if (error) {
        setError(error);
      }
      setPaginator(paginate);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [filters]);

  return (
    <LicenseListContext.Provider value={{ paginator, isLoading, error }}>
      <LicenseListFilter />
      <div className="card card-info">
        <div className="card-header">
          <div className="card-title text-capitalize">{trans('license_list_page')}</div>
          <div className="card-tools">
            <LicenseAddModal />
          </div>
        </div>
        <div className="card-body">
          {!!isLoading && <LoadingOverlay />}
          <LicenseListTable />
          <Pagination params={paginator} />
        </div>
      </div>
    </LicenseListContext.Provider>
  );
};
