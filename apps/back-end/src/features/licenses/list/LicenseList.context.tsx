import { LicenseDtos, PaginatorDto } from '@dtos';
import React from 'react';

const LicenseListContext = React.createContext<{
  paginator?: PaginatorDto<LicenseDtos.LicenseDto>;
  isLoading?: boolean;
  error?: string;
}>({});

export default LicenseListContext;
