import { from } from 'rxjs';
import { useFilters, useTrans } from '@back-end/hooks';
import { useContext, useEffect, useState, useMemo } from 'react';
import LicenseListContext from './LicenseList.context';
import { FormProvider, useForm } from 'react-hook-form';
import { LicenseDtos } from '@dtos';
import Form from '@react-libs/components/Form';
import { ISelectOptions } from '@react-libs/types';
import { agentApi } from '@back-end/apis';
import { enumToOptions } from '@react-libs/utils';
import { LICENSE_STATES } from '@utils';

export const LicenseListFilter = () => {
  const trans = useTrans();
  const { filters, pushFilters } = useFilters();

  const [agentOptions, setAgentOptions] = useState<ISelectOptions[]>();
  const trialOptions: ISelectOptions[] = [
    {
      value: '0',
      label: trans('paid'),
    },
    {
      value: 1,
      label: trans('trial'),
    },
  ];

  const stateOptions = useMemo((): ISelectOptions[] => {
    const options = enumToOptions(LICENSE_STATES, 'licenses_states').map((option) => ({
      ...option,
      label: trans(option.label),
    }));
    return options;
  }, []);

  const { isLoading } = useContext(LicenseListContext);

  const methods = useForm<LicenseDtos.SearchLicenseDto>({
    defaultValues: filters,
  });

  const { handleSubmit } = methods;

  const onSubmit = handleSubmit((data) => {
    pushFilters({ ...data });
  });

  useEffect(() => {
    const subscription = from(agentApi.fetchAll()).subscribe(({ agents }) => {
      if (!agents?.length) {
        setAgentOptions([]);
        return;
      }
      const options = agents.map((agent): ISelectOptions => ({ value: agent.id, label: agent.name }));

      setAgentOptions(options);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  return (
    <FormProvider {...methods}>
      <form method="GET" onSubmit={onSubmit} className="mb-3">
        <div className="grid-repeat-fill-20x">
          <Form.InputGroup prepend={<i className="fas fa-key"></i>}>
            <Form.InputBox name="licenseKey" disabled={isLoading} />
          </Form.InputGroup>

          {!!agentOptions?.length && (
            <Form.InputGroup prepend={<i className="fa fas fa-handshake"></i>}>
              <Form.Select2Box name="agentId" options={agentOptions} placeholder={trans('agent')} />
            </Form.InputGroup>
          )}

          {!!trialOptions?.length && (
            <Form.InputGroup prepend={<i className="fas fa-toggle-on"></i>}>
              <Form.Select2Box name="isTrial" options={trialOptions} placeholder={trans('type')} />
            </Form.InputGroup>
          )}

          {!!stateOptions?.length && (
            <Form.InputGroup prepend={<i className="fas fa-sign"></i>}>
              <Form.Select2Box name="state" options={stateOptions} />
            </Form.InputGroup>
          )}

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="activatedFromDate" disabled={isLoading} />
          </Form.InputGroup>

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="activatedToDate" disabled={isLoading} />
          </Form.InputGroup>

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="expiredFromDate" disabled={isLoading} />
          </Form.InputGroup>

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="expiredToDate" disabled={isLoading} />
          </Form.InputGroup>

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="fromDate" disabled={isLoading} />
          </Form.InputGroup>

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="toDate" disabled={isLoading} />
          </Form.InputGroup>

          <div>
            <Form.Button
              icon={<i className="fa fa-search mr-2"></i>}
              isSubmitting={isLoading}
              label={trans('search')}
            />
          </div>
        </div>
      </form>
    </FormProvider>
  );
};
