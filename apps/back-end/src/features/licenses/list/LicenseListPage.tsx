import { useHeader, useTrans } from '@back-end/hooks';
import { LicenseListContainer } from './LicenseList.container';

export const LicenseListPage = () => {
  const trans = useTrans();
  useHeader({
    pageTitle: trans('licenses'),
    section: 'licenses',
    breadcrumbs: [
      {
        title: trans('licenses'),
        link: '/licenses',
        active: true,
        icon: 'fas fa-handshake',
      },
    ],
  });

  return <LicenseListContainer />;
};
