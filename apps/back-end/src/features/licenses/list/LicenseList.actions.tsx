import { LicenseDtos } from '@dtos';
import { DropdownButton } from 'react-bootstrap';
import { LicenseEditModal } from '../edit';
import { LicenseDeleteModal } from '../delete';
import { LicenseRevokedModal } from '../revoked';
import { Link } from 'react-router-dom';
import { useTrans } from '@react-libs/hooks';

export const LicenseListActions = ({ license }: { license: LicenseDtos.LicenseDto }) => {
  const trans = useTrans();

  return (
    <DropdownButton title={<i className="fa fa-cog mr-2"></i>} size="sm">
      <Link to={`/licenses/${license.id}/view`} className="dropdown-item bg-info btn btn-sm">
        <i className="fa fa-eye mr-2"></i>
        {trans('view')}
      </Link>
      <LicenseEditModal license={license} className="dropdown-item bg-warning" />
      <LicenseRevokedModal license={license} className="dropdown-item bg-danger" />
      <LicenseDeleteModal license={license} className="dropdown-item bg-danger" />
    </DropdownButton>
  );
};
