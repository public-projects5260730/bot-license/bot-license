import { useTrans } from '@back-end/hooks';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { LicenseAddForm } from './LicenseAdd.form';

export const LicenseAddModal = ({ className }: { className?: string }) => {
  const trans = useTrans();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button className={`btn btn-danger btn-sm ${className}`} onClick={() => setShowModal(!showModal)}>
        <i className="fas fa-plus mr-2"></i>
        {trans('add')}
      </button>

      <Modal
        show={showModal}
        onHide={() => setShowModal(!showModal)}
        aria-labelledby="licenses-add"
        size="lg"
        centered
        animation={false}
      >
        <Modal.Header closeButton className="bg-info">
          <Modal.Title id="licenses-add" className="text-truncate">
            {trans('add_license')}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LicenseAddForm />
        </Modal.Body>
      </Modal>
    </>
  );
};
