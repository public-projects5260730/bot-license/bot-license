import { from } from 'rxjs';
import { useState, useEffect, useMemo } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useReloadPage, useTrans } from '@back-end/hooks';
import { agentApi, licenseApi } from '@back-end/apis';
import Form from '@back-end/components/Form';
import { LicenseDtos } from '@dtos';
import { ISelectOptions } from '@react-libs/types';
import { enumToOptions } from '@react-libs/utils';
import { LICENSE_STATES } from '@utils';

export const LicenseAddForm = () => {
  const trans = useTrans();
  const reloadPage = useReloadPage();

  const [agentOptions, setAgentOptions] = useState<ISelectOptions[]>();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [formError, setFormError] = useState<string>();

  useEffect(() => {
    const subscription = from(agentApi.fetchAll()).subscribe(({ agents }) => {
      if (!agents?.length) {
        setAgentOptions([]);
        return;
      }
      const options = agents.map((agent): ISelectOptions => ({ value: agent.id, label: agent.name }));

      setAgentOptions(options);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  const stateOptions = useMemo((): ISelectOptions[] => {
    const options = enumToOptions(LICENSE_STATES, 'licenses_states').map((option) => ({
      ...option,
      label: trans(option.label),
    }));
    return options;
  }, []);

  const methods = useForm<LicenseDtos.CreateLicenseDto>({
    defaultValues: {
      accountNumber: 1,
      licenseDays: 3,
      isTrial: true,
      state: LICENSE_STATES.NEW,
    },
  });
  const { handleSubmit, setError } = methods;

  const onSubmit = handleSubmit((formData) => {
    if (isSubmitting) {
      return;
    }
    setFormError('');
    setIsSubmitting(true);

    licenseApi.sendAdd(formData).then(({ license, errors, error }) => {
      setIsSubmitting(false);
      if (error) {
        setFormError(error);
      }
      if (errors) {
        Object.entries(errors).map(([field, message]) => {
          return setError(field as keyof LicenseDtos.CreateLicenseDto, {
            type: 'manual',
            message,
          });
        });
      }

      if (license?.id) {
        reloadPage('/licenses');
      }
    });
  });

  return (
    <FormProvider {...methods}>
      <form method="GET" onSubmit={onSubmit}>
        {!!formError?.length && <div className="callout callout-danger text-danger p-2 small">{formError}</div>}

        {!!agentOptions?.length && (
          <Form.Row name="agentId" label={trans('agent')}>
            <Form.InputGroup name="agentId" prepend={<i className="fa fas fa-handshake"></i>}>
              <Form.Select2Box name="agentId" options={agentOptions} placeholder={trans('agent')} />
            </Form.InputGroup>
          </Form.Row>
        )}

        <Form.Row name="accountNumber">
          <Form.InputGroup name="accountNumber" prepend={<i className="fa fa-sliders"></i>}>
            <Form.InputBox name="accountNumber" type="number" min={-1} step={1} />
          </Form.InputGroup>
          <span className="text-muted small">Enter 1 for unlimited and non-zero</span>
        </Form.Row>

        <Form.Row name="licenseDays">
          <Form.InputGroup name="licenseDays" prepend={<i className="fa fa-sliders"></i>}>
            <Form.InputBox name="licenseDays" type="number" min={-1} step={1} />
          </Form.InputGroup>
          <span className="text-muted small">Enter 1 for unlimited and non-zero</span>
        </Form.Row>

        <Form.SwitchBox name="isTrial" />

        <Form.Row name="state">
          <Form.InputGroup prepend={<i className="fas fa-sign"></i>}>
            <Form.Select2Box name="state" options={stateOptions} />
          </Form.InputGroup>
        </Form.Row>

        <Form.Row name="licenseOptions">
          <Form.InputGroup prepend={<i className="fas fa-code"></i>}>
            <Form.TextAreaBox name="licenseOptions" rows={3} />
          </Form.InputGroup>
          <span className="text-muted small">Enter JSON code</span>
        </Form.Row>

        <hr />
        <div className="text-right">
          <Form.Button isSubmitting={isSubmitting} />
        </div>
      </form>
    </FormProvider>
  );
};
