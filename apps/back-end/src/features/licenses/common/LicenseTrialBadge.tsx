import { useTrans } from '@back-end/hooks';
import { LicenseDtos } from '@dtos';

export const LicenseTrialBadge = ({ license }: { license: LicenseDtos.LicenseDto }) => {
  const trans = useTrans();

  if (license?.isTrial) {
    return <span className="badge badge-danger">{trans('trial')}</span>;
  }

  return <span className="badge badge-info">{trans('paid')}</span>;
};
