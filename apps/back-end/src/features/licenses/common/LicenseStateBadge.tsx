import { useTrans } from '@back-end/hooks';
import { licenseStateClass } from '@back-end/utils';
import { LicenseDtos } from '@dtos';
import { enumLabel } from '@react-libs/utils';
import { LICENSE_STATES } from '@utils';

export const LicenseStateBadge = ({ license }: { license: LicenseDtos.LicenseDto }) => {
  const trans = useTrans();

  const { state } = license;

  return (
    <span className={`badge ${licenseStateClass(state)}`}>
      {trans(enumLabel(LICENSE_STATES, state, 'licenses_states'))}
    </span>
  );
};
