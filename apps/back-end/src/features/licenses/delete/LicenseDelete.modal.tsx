import { useTrans } from '@back-end/hooks';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { LicenseDeleteForm } from './LicenseDelete.form';
import { LicenseDtos } from '@dtos';

export const LicenseDeleteModal = ({ license, className }: { license: LicenseDtos.LicenseDto; className?: string }) => {
  const trans = useTrans();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button className={`btn btn-danger btn-sm ${className}`} onClick={() => setShowModal(!showModal)}>
        <i className="fas fa-trash-alt mr-2"></i>
        {trans('delete')}
      </button>

      <Modal
        show={showModal}
        onHide={() => setShowModal(!showModal)}
        aria-labelledby="licenses-delete"
        centered
        animation={false}
      >
        <Modal.Header closeButton className="bg-danger">
          <Modal.Title id="licenses-delete" className="text-truncate">
            {trans('delete_license')}: {license.licenseKey}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LicenseDeleteForm license={license} />
        </Modal.Body>
      </Modal>
    </>
  );
};
