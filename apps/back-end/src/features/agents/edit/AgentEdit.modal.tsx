import { useTrans } from '@back-end/hooks';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { AgentEditForm } from './AgentEdit.form';
import { AgentDtos } from '@dtos';

export const AgentEditModal = ({ agent, className }: { agent: AgentDtos.AgentDto; className?: string }) => {
  const trans = useTrans();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button className={`btn btn-danger btn-sm ${className}`} onClick={() => setShowModal(!showModal)}>
        <i className="fa fa-edit mr-2"></i>
        {trans('edit')}
      </button>

      <Modal
        show={showModal}
        onHide={() => setShowModal(!showModal)}
        aria-labelledby="agents-edit"
        centered
        animation={false}
      >
        <Modal.Header closeButton className="bg-warning">
          <Modal.Title id="agents-edit" className="text-truncate">
            {trans('edit_agent')}: {agent.name}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AgentEditForm agent={agent} />
        </Modal.Body>
      </Modal>
    </>
  );
};
