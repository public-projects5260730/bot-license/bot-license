import { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useReloadPage, useTrans } from '@back-end/hooks';
import { agentApi } from '@back-end/apis';
import Form from '@back-end/components/Form';
import { AgentDtos } from '@dtos';

export const AgentEditForm = ({ agent }: { agent: AgentDtos.AgentDto }) => {
  const trans = useTrans();
  const reloadPage = useReloadPage();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [formError, setFormError] = useState<string>();

  const methods = useForm<AgentDtos.EditAgentDto>({
    defaultValues: agent,
  });
  const { handleSubmit, setError } = methods;

  const onSubmit = handleSubmit((formData) => {
    if (isSubmitting) {
      return;
    }
    setFormError('');
    setIsSubmitting(true);

    agentApi.sendEdit(agent.id, formData).then(({ successfully, errors, error }) => {
      setIsSubmitting(false);
      if (error) {
        setFormError(error);
      }
      if (errors) {
        Object.entries(errors).map(([field, message]) => {
          return setError(field as keyof AgentDtos.EditAgentDto, {
            type: 'manual',
            message,
          });
        });
      }

      if (successfully) {
        reloadPage('/agents');
      }
    });
  });

  return (
    <FormProvider {...methods}>
      <form method="GET" onSubmit={onSubmit}>
        {!!formError?.length && <div className="callout callout-danger text-danger p-2 small">{formError}</div>}

        <Form.Row name="name" isRequired>
          <Form.InputGroup name="name">
            <Form.InputBox name="name" isRequired disabled={isSubmitting} />
          </Form.InputGroup>
        </Form.Row>

        <Form.Row name="webHook">
          <Form.InputGroup name="webHook">
            <Form.InputBox
              name="webHook"
              placeholder="https://your-webhook"
              rules={{
                pattern: {
                  value: /^(ftp|http|https):\/\/[^ "]+$/,
                  message: trans('invalid_url_format'),
                },
              }}
              disabled={isSubmitting}
            />
          </Form.InputGroup>
        </Form.Row>

        <Form.SwitchBox name="isChangeSecretKey" disabled={isSubmitting} />

        <hr />
        <div className="text-right">
          <Form.Button isSubmitting={isSubmitting} />
        </div>
      </form>
    </FormProvider>
  );
};
