import { useTrans } from '@back-end/hooks';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { AgentDeleteForm } from './AgentDelete.form';
import { AgentDtos } from '@dtos';

export const AgentDeleteModal = ({ agent, className }: { agent: AgentDtos.AgentDto; className?: string }) => {
  const trans = useTrans();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button className={`btn btn-danger btn-sm ${className}`} onClick={() => setShowModal(!showModal)}>
        <i className="fas fa-trash-alt mr-2"></i>
        {trans('delete')}
      </button>

      <Modal
        show={showModal}
        onHide={() => setShowModal(!showModal)}
        aria-labelledby="agents-delete"
        centered
        animation={false}
      >
        <Modal.Header closeButton className="bg-danger">
          <Modal.Title id="agents-delete" className="text-truncate">
            {trans('delete_agent')}: {agent.name}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AgentDeleteForm agent={agent} />
        </Modal.Body>
      </Modal>
    </>
  );
};
