import { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useReloadPage, useTrans } from '@back-end/hooks';
import { agentApi } from '@back-end/apis';
import Form from '@back-end/components/Form';
import { AgentDtos } from '@dtos';

export const AgentDeleteForm = ({ agent }: { agent: AgentDtos.AgentDto }) => {
  const trans = useTrans();
  const reloadPage = useReloadPage();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [formError, setFormError] = useState<string>();

  const methods = useForm();
  const { handleSubmit, setError } = methods;

  const onSubmit = handleSubmit(() => {
    if (isSubmitting) {
      return;
    }
    setFormError('');
    setIsSubmitting(true);

    agentApi.sendDelete(agent.id).then(({ successfully, error }) => {
      setIsSubmitting(false);
      if (error) {
        setFormError(error);
      }

      if (successfully) {
        reloadPage('/agents');
      }
    });
  });

  return (
    <FormProvider {...methods}>
      <form method="GET" onSubmit={onSubmit}>
        {!!formError?.length && <div className="callout callout-danger text-danger p-2 small">{formError}</div>}

        <div className="callout callout-danger">
          <h4>
            {trans('please_submit_to_delete_agent')}:<b className="ml-2">{agent.name}</b>
          </h4>
        </div>

        <hr />
        <div className="text-right mt-2">
          <Form.Button
            className="btn btn-danger"
            isSubmitting={isSubmitting}
            label={trans('delete')}
            icon={<i className="fas fa-trash-alt mr-2"></i>}
          />
        </div>
      </form>
    </FormProvider>
  );
};
