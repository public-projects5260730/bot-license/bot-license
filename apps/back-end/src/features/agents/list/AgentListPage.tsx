import { useHeader, useTrans } from '@back-end/hooks';
import { AgentListContainer } from './AgentList.container';

export const AgentListPage = () => {
  const trans = useTrans();
  useHeader({
    pageTitle: trans('agents'),
    section: 'agents',
    breadcrumbs: [
      {
        title: trans('agents'),
        link: '/agents',
        active: true,
        icon: 'fas fa-handshake',
      },
    ],
  });

  return <AgentListContainer />;
};
