import { agentApi } from '@back-end/apis';
import { useFilters, useTrans } from '@back-end/hooks';
import { AgentDtos, PaginatorDto } from '@dtos';
import { useEffect, useState } from 'react';
import { from } from 'rxjs';
import AgentListContext from './AgentList.context';
import { AgentListFilter } from './AgentList.filter';
import { AgentListTable } from './AgentList.table';
import { LoadingOverlay, Pagination } from '@react-libs/components/Common';
import { AgentAddModal } from '../add';

export const AgentListContainer = () => {
  const trans = useTrans();
  const [paginator, setPaginator] = useState<PaginatorDto<AgentDtos.AgentDto>>();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string>();

  const { filters } = useFilters();

  useEffect(() => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);

    const subscription = from(agentApi.fetchPaginator(filters)).subscribe(({ paginate, error }) => {
      setIsLoading(false);
      if (error) {
        setError(error);
      }
      setPaginator(paginate);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [filters]);

  return (
    <AgentListContext.Provider value={{ paginator, isLoading, error }}>
      <AgentListFilter />
      <div className="card card-info">
        <div className="card-header">
          <div className="card-title text-capitalize">{trans('agent_list_page')}</div>
          <div className="card-tools">
            <AgentAddModal />
          </div>
        </div>
        <div className="card-body">
          {!!isLoading && <LoadingOverlay />}
          <AgentListTable />
          <Pagination params={paginator} />
        </div>
      </div>
    </AgentListContext.Provider>
  );
};
