import { useTrans } from '@back-end/hooks';
import { useContext } from 'react';
import AgentListContext from './AgentList.context';
import { strProtect } from '@utils';
import { toast } from 'react-toastify';
import { AgentListActions } from './AgentList.actions';
import { formatDate, tooltip } from '@back-end/utils';
import { RowNoData } from '@react-libs/components/Common';

export const AgentListTable = () => {
  const trans = useTrans();
  const { paginator, error } = useContext(AgentListContext);

  return (
    <div>
      {!!error?.length && <div className="callout callout-danger text-danger p-2 small">{error}</div>}
      <div className="table-responsive">
        <table className="table table-hover table-striped text-nowrap table-layout-fixed">
          <thead className="text-center">
            <tr>
              <th className="w-50 text-left">{trans('name')}</th>
              <th>{trans('secret_key')}</th>
              <th>{trans('created_at')}</th>
              <th className="text-right">{trans('action')}</th>
            </tr>
          </thead>
          <tbody className="text-center">
            {!paginator?.items?.length && <RowNoData />}

            {paginator?.items?.map((item) => (
              <tr key={item.id}>
                <td className="text-left">{item.name}</td>
                <td>
                  {strProtect(item.secretKey)}
                  <i
                    className="fas fa-copy ml-2 cursor-pointer"
                    {...tooltip(trans('copied'))}
                    onClick={() => {
                      navigator.clipboard.writeText(item.secretKey);
                      toast.dismiss();
                      toast.success(trans('copied'));
                    }}
                  ></i>
                </td>
                <td>{formatDate(item.createdAt)}</td>
                <td className="text-right">
                  <AgentListActions agent={item} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
