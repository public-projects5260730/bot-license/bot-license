import { AgentDtos } from '@dtos';
import { DropdownButton } from 'react-bootstrap';
import { AgentEditModal } from '../edit';
import { AgentDeleteModal } from '../delete';

export const AgentListActions = ({ agent }: { agent: AgentDtos.AgentDto }) => {
  return (
    <DropdownButton title={<i className="fa fa-cog mr-2"></i>} size="sm">
      <AgentEditModal agent={agent} className="dropdown-item bg-warning" />
      <AgentDeleteModal agent={agent} className="dropdown-item bg-danger" />
    </DropdownButton>
  );
};
