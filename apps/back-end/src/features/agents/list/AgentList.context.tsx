import { AgentDtos, PaginatorDto } from '@dtos';
import React from 'react';

const AgentListContext = React.createContext<{
  paginator?: PaginatorDto<AgentDtos.AgentDto>;
  isLoading?: boolean;
  error?: string;
}>({});

export default AgentListContext;
