import { useFilters, useTrans } from '@back-end/hooks';
import { useContext } from 'react';
import AgentListContext from './AgentList.context';
import { FormProvider, useForm } from 'react-hook-form';
import { AgentDtos } from '@dtos';
import Form from '@react-libs/components/Form';

export const AgentListFilter = () => {
  const trans = useTrans();
  const { filters, pushFilters } = useFilters();

  const { isLoading } = useContext(AgentListContext);

  const methods = useForm<AgentDtos.SearchAgentDto>({
    defaultValues: filters,
  });

  const { handleSubmit } = methods;

  const onSubmit = handleSubmit((data) => {
    pushFilters({ ...data });
  });

  return (
    <FormProvider {...methods}>
      <form method="GET" onSubmit={onSubmit} className="mb-3">
        <div className="grid-repeat-fill-20x">
          <Form.InputGroup prepend={<i className="fa fa-search"></i>}>
            <Form.InputBox name="name" disabled={isLoading} />
          </Form.InputGroup>

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="fromDate" disabled={isLoading} />
          </Form.InputGroup>

          <Form.InputGroup prepend={<i className="fa fa-calendar-o"></i>}>
            <Form.DatepickerBox name="toDate" disabled={isLoading} />
          </Form.InputGroup>

          <div>
            <Form.Button
              icon={<i className="fa fa-search mr-2"></i>}
              isSubmitting={isLoading}
              label={trans('search')}
            />
          </div>
        </div>
      </form>
    </FormProvider>
  );
};
