import { useTrans } from '@back-end/hooks';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { AgentAddForm } from './AgentAdd.form';

export const AgentAddModal = ({ className }: { className?: string }) => {
  const trans = useTrans();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button className={`btn btn-danger btn-sm ${className}`} onClick={() => setShowModal(!showModal)}>
        <i className="fas fa-plus mr-2"></i>
        {trans('add')}
      </button>

      <Modal
        show={showModal}
        onHide={() => setShowModal(!showModal)}
        aria-labelledby="agents-add"
        centered
        animation={false}
      >
        <Modal.Header closeButton className="bg-info">
          <Modal.Title id="agents-add" className="text-truncate">
            {trans('add_agent')}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AgentAddForm />
        </Modal.Body>
      </Modal>
    </>
  );
};
