import { useHeader, useTrans } from '@back-end/hooks';
import { LicenseStatisticsContainer } from '../licenses/statistics';

export const HomePage = () => {
  const trans = useTrans();

  useHeader({
    pageTitle: trans('home'),
    section: 'home',
  });

  return <LicenseStatisticsContainer />;
};
