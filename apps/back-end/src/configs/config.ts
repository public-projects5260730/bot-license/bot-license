let config: {
  siteName: string;
  apiUrl: string;
} = {
  siteName: '',
  apiUrl: '',
};

if (process.env?.['NODE_ENV'] === 'development') {
  config = {
    siteName: process.env?.['NX_BACK_END_SITE_NAME'] || '',
    apiUrl: process.env?.['NX_BACK_END_API_URL'] || '',
  };
} else if (typeof window?._env !== 'undefined') {
  config = {
    siteName: window?._env?.NX_BACK_END_SITE_NAME || '',
    apiUrl: window?._env?.NX_BACK_END_API_URL || '',
  };
}

export default config;
