import { LicenseDtos } from '@dtos';

export interface IEditLicenseFormData extends Omit<LicenseDtos.EditLicenseDto, 'licenseOptions'> {
  licenseOptions?: Record<string, unknown> | string;
}
