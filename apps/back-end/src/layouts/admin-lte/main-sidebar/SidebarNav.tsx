import { useAppSelector, useTrans } from '@back-end/hooks';
import { selectVisitor } from '@back-end/store/auth';
import { ISidebarNav } from '@back-end/types';
import SidebarAuth from './SidebarAuth';
import SidebarNavItem from './SidebarNavItem';

const SidebarNav = () => {
  const trans = useTrans();

  const visitor = useAppSelector(selectVisitor);

  const navList: ISidebarNav[] = [
    {
      title: trans('home'),
      link: '/',
      session: 'home',
      icon: 'fa fa-home',
      visible: !!visitor?.id,
    },
    {
      title: trans('agents'),
      link: '/agents',
      session: 'agents',
      icon: 'fas fa-handshake',
      visible: !!visitor?.id,
    },
    {
      title: trans('licenses'),
      link: '/licenses',
      session: 'licenses',
      icon: 'fas fa-key',
      visible: !!visitor?.id,
    },
  ];

  return (
    <nav className="mt-2">
      <ul className="nav nav-pills nav-sidebar flex-column nav-flat">
        {navList.map((nav, i) => (
          <SidebarNavItem nav={nav} key={i} />
        ))}
        <SidebarAuth />
      </ul>
    </nav>
  );
};

export default SidebarNav;
