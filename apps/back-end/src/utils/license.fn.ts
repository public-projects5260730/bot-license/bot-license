import { LICENSE_STATES } from '.';

export const licenseStateClass = (state: number, prefix = 'bg-gradient-'): string => {
  switch (state) {
    case LICENSE_STATES.NEW:
      return `${prefix}info`;
    case LICENSE_STATES.ACTIVATED:
      return `${prefix}success`;
    case LICENSE_STATES.EXPIRED:
      return `${prefix}warning`;
    case LICENSE_STATES.CANCELED:
      return `${prefix}secondary`;
    default:
      return `${prefix}danger`;
  }
};
