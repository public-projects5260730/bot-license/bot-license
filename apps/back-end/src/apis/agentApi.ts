import { AgentDtos, PaginatorDto } from '@dtos';
import axiosClient from './axiosClient';
import { IApiResponse } from '@react-libs/types';

const prefix = '/agents';

export const agentApi = {
  fetchAll: (): Promise<IApiResponse<{ agents?: AgentDtos.AgentDto[] }>> => {
    const url = `${prefix}/all`;
    return axiosClient.get(url);
  },

  fetchPaginator: (
    params?: AgentDtos.SearchAgentDto
  ): Promise<IApiResponse<{ paginate?: PaginatorDto<AgentDtos.AgentDto> }>> => {
    const url = `${prefix}`;
    return axiosClient.get(url, { params });
  },

  fetchView: (id: number): Promise<IApiResponse<{ agent?: AgentDtos.AgentDto }>> => {
    const url = `${prefix}/${id}/view`;
    return axiosClient.get(url);
  },

  sendAdd: (params?: AgentDtos.CreateAgentDto): Promise<IApiResponse<{ agent?: AgentDtos.AgentDto }>> => {
    const url = `${prefix}/add`;
    return axiosClient.post(url, params);
  },

  sendEdit: (
    id: number,
    params?: AgentDtos.EditAgentDto
  ): Promise<IApiResponse<{ agent?: AgentDtos.AgentDto; successfully?: boolean }>> => {
    const url = `${prefix}/${id}/edit`;
    return axiosClient.post(url, params);
  },

  sendDelete: (id: number): Promise<IApiResponse<{ successfully?: boolean }>> => {
    const url = `${prefix}/${id}/delete`;
    return axiosClient.post(url);
  },
};
