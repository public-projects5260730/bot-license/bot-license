import { LicenseDtos, PaginatorDto } from '@dtos';
import axiosClient from './axiosClient';
import { IApiResponse } from '@react-libs/types';
import { IEditLicenseFormData } from '@back-end/types';

const prefix = '/licenses';

export const licenseApi = {
  fetchPaginator: (
    params?: LicenseDtos.SearchLicenseDto
  ): Promise<IApiResponse<{ paginate?: PaginatorDto<LicenseDtos.LicenseDto> }>> => {
    const url = `${prefix}`;
    return axiosClient.get(url, { params });
  },

  fetchStatistics: (): Promise<IApiResponse<{ statistics?: LicenseDtos.LicenseStatisticsDto[] }>> => {
    const url = `${prefix}/statistics`;
    return axiosClient.get(url);
  },

  fetchView: (id: number): Promise<IApiResponse<{ license?: LicenseDtos.LicenseDto }>> => {
    const url = `${prefix}/${id}/view`;
    return axiosClient.get(url);
  },

  sendAdd: (params?: LicenseDtos.CreateLicenseDto): Promise<IApiResponse<{ license?: LicenseDtos.LicenseDto }>> => {
    const url = `${prefix}/add`;
    return axiosClient.post(url, params);
  },

  sendEdit: (
    id: number,
    params?: IEditLicenseFormData
  ): Promise<IApiResponse<{ license?: LicenseDtos.LicenseDto; successfully?: boolean }>> => {
    const url = `${prefix}/${id}/edit`;
    return axiosClient.post(url, params);
  },

  sendDelete: (id: number): Promise<IApiResponse<{ successfully?: boolean }>> => {
    const url = `${prefix}/${id}/delete`;
    return axiosClient.post(url);
  },
};
