import axiosClient from './axiosClient';
import { IApiResponse, ILoginData, IUser } from '@back-end/types';

export const authApi = {
  fetchRefreshToken: (): Promise<IApiResponse<{ accessToken?: string }>> => {
    const url = `/auth/refresh-token`;
    return axiosClient.post(url);
  },

  fetchVisitor: (): Promise<
    IApiResponse<{
      visitor?: IUser;
    }>
  > => {
    return axiosClient.get('/auth/me');
  },

  sendLogin: (params: ILoginData): Promise<IApiResponse<{ accessToken?: string }>> => {
    const url = `/auth/login`;
    return axiosClient.post(url, params);
  },
};
