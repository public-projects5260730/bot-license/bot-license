import { BotLicenseSubscribeDtos } from '@dtos';
import { LoggerService } from '@logger';
import { HttpStatus, Inject, UseGuards } from '@nestjs/common';
import { MessageBody, SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { BOT_LICENSE_EVENTS, LICENSE_STATES } from '@utils';
import { plainClass } from '@nest-utils';
import { SocketPassKeyGuard } from '@guards';
import { AppConfig } from './app.config';
import { validate, ValidationError } from 'class-validator';
import { ActivatedLicenseRepoInterface, LicenseRepoInterface } from '@repositories';
import { In } from 'typeorm';
import { Socket } from 'net';

@WebSocketGateway(AppConfig.gatewayPort)
@UseGuards(SocketPassKeyGuard)
export class AppGateway {
  constructor(
    @Inject('REPOSITORIES.LICENSE')
    private readonly licenseRepo: LicenseRepoInterface,

    @Inject('REPOSITORIES.ACTIVATEDLICENSE')
    private readonly activatedLicenseRepo: ActivatedLicenseRepoInterface,

    @Inject(LoggerService)
    private readonly logger: LoggerService
  ) {
    this.logger.setContext('gateways');
  }

  handleConnection(client: Socket) {
    client.setTimeout(AppConfig.gatewayTimeout * 1000);
    client.on('timeout', () => {
      this.logger.log('Socket Timeout Event.', { client });
    });

    client.on('error', (err) => {
      this.logger.log('Socket Error Event.', { err });
    });
  }

  @SubscribeMessage(BOT_LICENSE_EVENTS.BOT_STARTED)
  async handleCheckLicense(@MessageBody() data: BotLicenseSubscribeDtos.CheckLicenseDto) {
    const checkLicenseData = plainClass(BotLicenseSubscribeDtos.CheckLicenseDto, data);
    const errors = await validate(checkLicenseData);
    if (errors?.length) {
      return this.errors(errors);
    }

    const license = await this.licenseRepo.findOne({
      where: {
        licenseKey: checkLicenseData.licenseKey,
      },
      relations: {
        activatedLicenses: true,
      },
    });

    if (!license) {
      return this.error(`Requested license "${checkLicenseData.licenseKey}" not found.`, HttpStatus.NOT_FOUND);
    }

    if (license.state == LICENSE_STATES.EXPIRED) {
      return this.error(`The license "${checkLicenseData.licenseKey}" has expired.`);
    } else if (license.state == LICENSE_STATES.CANCELED) {
      return this.error(`The license "${checkLicenseData.licenseKey}" has canceled.`);
    } else if (license.state == LICENSE_STATES.REVOKED) {
      return this.error(`The license "${checkLicenseData.licenseKey}" has canceled.`);
    }

    if (license.licenseDays >= 0 && license?.expiredAt && Date.now() > license?.expiredAt?.getTime()) {
      await this.licenseRepo.update({ licenseKey: checkLicenseData.licenseKey }, { state: LICENSE_STATES.EXPIRED });
      return this.error(`The license "${checkLicenseData.licenseKey}" has expired.`);
    }

    const isAccountActivated = !!license?.activatedLicenses.find(
      ({ accountId }) => accountId == checkLicenseData?.accountId
    );

    if (isAccountActivated) {
      return this.successfully({ license: await this.licenseRepo.mergeLicenseOptions(license, true) });
    }

    if (license?.accountNumber >= 0 && license?.activatedLicenses?.length >= license.accountNumber) {
      return this.error(`This license has reached account limit.`);
    }

    const activatedLicense = await this.activatedLicenseRepo
      .createOrFail({ ...checkLicenseData, licenseId: license.id })
      .catch((error) => {
        this.logger.error('Insert Activated license', { license, checkLicenseData, error: error.toString() });
      });

    if (!activatedLicense) {
      return this.error(`Insert Activated license fail`);
    }

    const newLicense = await this.licenseRepo.findOneBy({
      licenseKey: checkLicenseData.licenseKey,
      state: In([LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED]),
    });

    return this.successfully({ license: await this.licenseRepo.mergeLicenseOptions(newLicense, true) });
  }

  private errors = (validationErrors: ValidationError[] = [], status: number = HttpStatus.BAD_REQUEST) => {
    const errors = validationErrors.reduce(
      (errors, error) => ({
        ...errors,
        [error.property]: Object.values(error.constraints).join('\n'),
      }),
      {}
    );

    return {
      status,
      successfully: false,
      event: BOT_LICENSE_EVENTS.ERRORS,
      error: 'Validation Params',
      errors,
    };
  };

  private error = (error?: string, status: number = HttpStatus.FORBIDDEN) => {
    return {
      status,
      successfully: false,
      event: BOT_LICENSE_EVENTS.ERRORS,
      error: error ?? 'You do not have permission to view this page or perform this action.',
      errors: {},
    };
  };

  private successfully = (data: Record<string, unknown>, status: number = HttpStatus.OK) => {
    return {
      status,
      successfully: true,
      event: BOT_LICENSE_EVENTS.LICENSE_RESPONSE,
      error: '',
      errors: {},
      ...data,
    };
  };
}
