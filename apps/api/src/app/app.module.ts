import { AuthModule } from './auth/auth.module';
import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { LoggerModule } from '@logger';
import { GuardsModule } from '@guards';
import { RepositoriesModule } from '@repositories';
import { AgentModule } from './agent/agent.module';
import { AppGateway } from './app.gateway';
import { LicenseModule } from './license/license.module';
import { AgentLicenseModule } from './agent-license/agent-license.module';
import { CheckLicenseModule } from './check-license/check-license.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    RepositoriesModule.forRoot(),
    LoggerModule.forRoot(),
    GuardsModule,
    AuthModule,
    AgentModule,
    LicenseModule,
    AgentLicenseModule,
    CheckLicenseModule,
  ],
  providers: [AppGateway],
})
export class AppModule {}
