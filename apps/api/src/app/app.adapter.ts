import { WebSocketAdapter, INestApplicationContext, Logger } from '@nestjs/common';
import { MessageMappingProperties } from '@nestjs/websockets';
import { Observable, fromEvent, EMPTY, from } from 'rxjs';
import { mergeMap, filter, buffer, map, tap } from 'rxjs/operators';
import { Server, Socket, createServer } from 'net';
import { BotLicenseSubscribeDtos } from '@dtos';
import { plainClass } from '@nest-utils';

export class AppAdapter implements WebSocketAdapter {
  constructor(private app: INestApplicationContext) {}

  create(port: number, options: any = {}): any {
    const server = createServer();
    server.listen(port, options.hostname, () => {
      Logger.log('Gateway tcp start at ::' + port);
    });
    return server;
  }

  bindClientConnect(server: Server, callback: (socket: Socket) => void) {
    server.on('connection', callback);
    server.on('error', (err) => {
      console.log({
        socketEvent: 'error',
        err,
      });
    });
  }

  bindMessageHandlers(client: Socket, handlers: MessageMappingProperties[], process: (data: any) => Observable<any>) {
    const source$ = fromEvent(client, 'data');
    source$
      .pipe(
        map((data) => data.toString().trim()),
        // map(() => this.fakeData()),
        // tap((fakeData) => console.log({ fakeData })),
        buffer(
          source$.pipe(
            map((data) => data.toString().trim()),
            filter((data: string) => !!data.match(/\s*;{3,}\s*$/))
          )
        ),
        map((messages) => messages.join('')),
        mergeMap((data) => from(this.parseMessage(data))),
        mergeMap((message) => this.bindMessageHandler(message, handlers, process)),
        filter((result) => result)
      )
      .subscribe((response) => {
        try {
          const res = plainClass(BotLicenseSubscribeDtos.LicenseResponseDto, response);
          return client.write(JSON.stringify(res) + ';;;');
        } catch (error) {
          console.log({
            method: 'bindMessageHandlers',
            client,
            response,
          });
          return;
        }
      });

    fromEvent(client, 'end')
      .pipe(
        mergeMap((message) => {
          const eventName = 'end';
          const messageHandler = handlers.find(({ message }) => message === eventName);
          if (!messageHandler) {
            return EMPTY;
          }
          return process(messageHandler.callback(message));
        })
      )
      .subscribe(() => null);
  }

  parseMessage(data: string) {
    return data.split(/\s*;{3,}\s*/);
  }

  bindMessageHandler(
    buffer,
    handlers: MessageMappingProperties[],
    process: (data: any) => Observable<any>
  ): Observable<any> {
    if (!buffer?.length) {
      return EMPTY;
    }

    let message;
    try {
      message = JSON.parse(buffer);
    } catch (error) {
      console.log({
        method: 'bindMessageHandler',
        bufferString: buffer.toString().trim(),
      });
      return EMPTY;
    }
    const eventName = message.event || 'other';
    const messageHandler = handlers.find((handler) => handler.message === eventName);
    if (!messageHandler) {
      return EMPTY;
    }

    return process(messageHandler.callback(message));
  }

  close(server: Server) {
    server.close();
  }

  private fakeData() {
    const data = {
      event: 'BOT_STARTED',
      accountId: 44211041,
      licenseKey: 'RG2YV-RL6IJ-9MR3Q-GXJTR-NDZ9S',
    };

    return JSON.stringify(data) + ';;;';
  }
}
