import { Body, Controller, Get, Inject, Param, ParseIntPipe, Post, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '@guards';
import { AgentDtos, PaginateDto } from '@dtos';
import { AgentRepoInterface } from '@repositories';
import { pagination } from '@nest-utils';
import { plainClass } from '@nest-utils';

@Controller('agents')
@UseGuards(JwtAuthGuard)
export class AgentController {
  constructor(@Inject('REPOSITORIES.AGENT') private readonly repo: AgentRepoInterface) {}

  @Get('all')
  async all() {
    const items = await this.repo.find();
    const agents = plainClass(AgentDtos.AgentDto, items);
    return { agents };
  }

  @Get()
  async index(@Query() paginate: PaginateDto, @Query() filters: AgentDtos.SearchAgentDto) {
    const where = this.repo.findSearchWhere(filters);
    const [items, total] = await this.repo.paginate(paginate, { where });
    const agents = plainClass(AgentDtos.AgentDto, items);
    return { paginate: pagination(agents, total, paginate.page, paginate.perPage) };
  }

  @Post('add')
  async add(@Body() inputs: AgentDtos.CreateAgentDto) {
    const secretKey = await this.repo.generateSecretKey();
    const agent = await this.repo.createOrFail({ ...inputs, secretKey });

    return { agent: plainClass(AgentDtos.AgentDto, agent) };
  }

  @Get(':id/view')
  async view(@Param('id', ParseIntPipe) id: number) {
    const agent = await this.repo.findByIdOrFail(id);

    return { agent: plainClass(AgentDtos.AgentDto, agent) };
  }

  @Post(':id/edit')
  async edit(@Param('id', ParseIntPipe) id: number, @Body() inputs: AgentDtos.EditAgentDto) {
    await this.repo.findByIdOrFail(id);

    let secretKey: string;
    if (inputs?.isChangeSecretKey) {
      secretKey = await this.repo.generateSecretKey();
    }
    const result = await this.repo.updateOrFail(id, { ...inputs, secretKey });

    const agent = await this.repo.findByIdOrFail(id);
    return { successfully: !!result?.affected, agent: plainClass(AgentDtos.AgentDto, agent) };
  }

  @Post(':id/delete')
  async delete(@Param('id', ParseIntPipe) id: number) {
    await this.repo.findByIdOrFail(id);
    const result = await this.repo.delete({ id });

    return { successfully: !!result?.affected };
  }
}
