import { AgentLicenseDtos, LicenseDtos, PaginateDto } from '@dtos';
import { AgentSecretKeyGuard } from '@guards';
import { pagination } from '@nest-utils';
import { Body, Controller, Get, Inject, Post, Query, Req, UseGuards } from '@nestjs/common';
import { AgentLicenseOptionRepoInterface, LicenseRepoInterface } from '@repositories';
import { LICENSE_STATES } from '@utils';
import { plainClass } from '@nest-utils';
import { In } from 'typeorm';

@Controller('agent-licenses')
@UseGuards(AgentSecretKeyGuard)
export class AgentLicenseController {
  constructor(
    @Inject('REPOSITORIES.LICENSE')
    private readonly repo: LicenseRepoInterface,

    @Inject('REPOSITORIES.AGENTLICENSEOPTION')
    private readonly licenOptionRepo: AgentLicenseOptionRepoInterface
  ) {}

  @Get()
  async index(@Req() { agent }, @Query() paginate: PaginateDto) {
    const [items, total] = await this.repo.paginate(paginate, { where: { agentId: agent.id } });
    return pagination(items, total, paginate.page, paginate.perPage);
  }

  @Post('list')
  async list(@Req() { agent }, @Body() inputs: AgentLicenseDtos.SearchLicenseList) {
    const entities = await this.repo.find({
      where: { agentId: agent.id, licenseKey: In(inputs.licenseKeys) },
      relations: {
        activatedLicenses: true,
      },
    });

    const licenses = plainClass(LicenseDtos.LicenseDto, entities);
    return { licenses };
  }

  @Post('add')
  async add(@Req() { agent }, @Body() inputs: AgentLicenseDtos.CreateLicenseDto) {
    const licenseKey = await this.repo.generateLicenseKey();
    const license = await this.repo.createOrFail({ ...inputs, licenseKey, agentId: agent.id });

    return { license: plainClass(LicenseDtos.LicenseDto, license) };
  }

  @Post('edit')
  async edit(@Req() { agent }, @Body() inputs: AgentLicenseDtos.EditLicenseDto) {
    const orgLicense = await this.repo.findOneByOrFail({
      agentId: agent.id,
      licenseKey: inputs.licenseKey,
      state: In([LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED]),
    });
    const result = await this.repo.updateOrFail(orgLicense.id, inputs);
    const license = await this.repo.findByIdOrFail(orgLicense.id);

    return { successfully: !!result?.affected, license: plainClass(LicenseDtos.LicenseDto, license) };
  }

  @Post('cancel')
  async cancel(@Req() { agent }, @Body() inputs: AgentLicenseDtos.CancelLicenseDto) {
    const orgLicense = await this.repo.findOneByOrFail({
      agentId: agent.id,
      licenseKey: inputs.licenseKey,
      state: In([LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED]),
    });
    const result = await this.repo.updateOrFail(orgLicense.id, { ...inputs, state: LICENSE_STATES.CANCELED });
    const license = await this.repo.findByIdOrFail(orgLicense.id);

    return { successfully: !!result?.affected, license: plainClass(LicenseDtos.LicenseDto, license) };
  }

  @Post('options')
  async options(@Req() { agent }, @Body() inputs: AgentLicenseDtos.OptionsDto) {
    const upsert = await this.licenOptionRepo.upsert({ ...inputs, agentId: agent.id }, ['options']).catch(() => null);
    return { successfully: !!upsert?.identifiers?.some(({ id }) => !!id) };
  }
}
