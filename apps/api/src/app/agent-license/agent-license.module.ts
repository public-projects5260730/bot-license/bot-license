import { Module } from '@nestjs/common';
import { AgentLicenseController } from './agent-license.controller';

@Module({
  controllers: [AgentLicenseController],
})
export class AgentLicenseModule {}
