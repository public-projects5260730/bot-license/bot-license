import { User } from '@entity';
import { Inject, Injectable } from '@nestjs/common';
import { compare } from 'bcrypt';
import { LoginDto, RegisterDto } from '@dtos';
import { GuardJwtService } from '@guards';
import { UserRepoInterface } from '@repositories';

@Injectable()
export class AuthService {
  @Inject('REPOSITORIES.USER')
  private repo: UserRepoInterface;

  @Inject(GuardJwtService)
  private jwtService: GuardJwtService;

  async validateUser(loginData: LoginDto): Promise<User | undefined> {
    const user = await this.repo.findOneBy({ email: loginData.email });
    if (!user) {
      return;
    }

    const check = await compare(loginData.password, user.password);
    if (!check) {
      return;
    }

    return user;
  }

  async register(registerData: RegisterDto): Promise<User | undefined> {
    return this.repo.register(registerData);
  }

  sign(userDto) {
    return this.jwtService.sign(userDto);
  }

  findUser = (id: number) => {
    return this.repo.findByIdOrFail(id);
  };
}
