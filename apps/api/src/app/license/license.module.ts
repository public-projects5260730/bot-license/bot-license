import { Module } from '@nestjs/common';
import { LicenseController } from './license.controller';
import { LicenseSchedule } from './license.schedule';

@Module({
  controllers: [LicenseController],
  providers: [LicenseSchedule],
})
export class LicenseModule {}
