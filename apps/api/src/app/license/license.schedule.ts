import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { LicenseRepoInterface } from '@repositories';
import { LICENSE_STATES, addDays } from '@utils';
import { LessThanOrEqual, MoreThanOrEqual } from 'typeorm';

@Injectable()
export class LicenseSchedule {
  constructor(@Inject('REPOSITORIES.LICENSE') private readonly repo: LicenseRepoInterface) {}

  @Cron(CronExpression.EVERY_MINUTE)
  async updateExpired() {
    await this.repo.update(
      {
        state: LICENSE_STATES.ACTIVATED,
        expiredAt: LessThanOrEqual(new Date()),
        licenseDays: MoreThanOrEqual(0),
      },
      {
        state: LICENSE_STATES.EXPIRED,
      }
    );
  }

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  async removeTrialLicenseExpired() {
    await this.repo.delete({
      isTrial: true,
      state: LICENSE_STATES.EXPIRED,
      expiredAt: LessThanOrEqual(addDays(new Date(), -7)),
    });
  }
}
