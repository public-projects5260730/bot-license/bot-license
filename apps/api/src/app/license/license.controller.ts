import { LicenseDtos, PaginateDto } from '@dtos';
import { JwtAuthGuard } from '@guards';
import { pagination, plainClass, repo } from '@nest-utils';
import { Body, Controller, Get, Inject, Param, ParseIntPipe, Post, Query, UseGuards } from '@nestjs/common';
import { LicenseRepoInterface } from '@repositories';

@Controller('licenses')
@UseGuards(JwtAuthGuard)
export class LicenseController {
  constructor(
    @Inject('REPOSITORIES.LICENSE')
    private readonly repo: LicenseRepoInterface
  ) {}

  @Get()
  async index(@Query() paginate: PaginateDto, @Query() filters: LicenseDtos.SearchLicenseDto) {
    const where = this.repo.findSearchWhere(filters);
    const [items, total] = await this.repo.paginate(paginate, {
      where,
      relations: { agent: true, activatedLicenses: true },
      order: {
        createdAt: 'DESC',
      },
    });
    const licenses = plainClass(LicenseDtos.LicenseDto, items);

    return { paginate: pagination(licenses, total, paginate.page, paginate.perPage) };
  }

  @Get('statistics')
  async statistics() {
    const statistics = await this.repo.findStatistics();
    return { statistics };
  }

  @Post('add')
  async add(@Body() inputs: LicenseDtos.CreateLicenseDto) {
    const licenseKey = await this.repo.generateLicenseKey();
    const license = await this.repo.createOrFail({ ...inputs, licenseKey });

    return { license: plainClass(LicenseDtos.LicenseDto, license) };
  }

  @Get(':id/view')
  async view(@Param('id', ParseIntPipe) id: number) {
    const license = await this.repo.findOneOrFail({
      where: { id },
      relations: { agent: true, activatedLicenses: true },
    });

    const _license = await this.repo.mergeLicenseOptions(license);
    return {
      license: plainClass(LicenseDtos.LicenseDto, _license, {
        excludeExtraneousValues: false,
      }),
    };
  }

  @Post(':id/edit')
  async edit(@Param('id', ParseIntPipe) id: number, @Body() inputs: LicenseDtos.EditLicenseDto) {
    await this.repo.findByIdOrFail(id);
    const result = await this.repo.updateOrFail(id, inputs);
    const license = await this.repo.findOneOrFail({
      where: { id },
      relations: { agent: true, activatedLicenses: true },
    });

    return { successfully: !!result?.affected, license: plainClass(LicenseDtos.LicenseDto, license) };
  }

  @Post(':id/delete')
  async delete(@Param('id', ParseIntPipe) id: number) {
    await this.repo.findByIdOrFail(id);
    const result = await this.repo.delete({ id });
    return { successfully: !!result?.affected };
  }
}
