import { Module } from '@nestjs/common';
import { CheckLicenseController } from './check-license.controller';

@Module({
  controllers: [CheckLicenseController],
})
export class CheckLicenseModule {}
