import { BotLicenseSubscribeDtos, LicenseDtos } from '@dtos';
import { CheckLicensePassKeyGuard } from '@guards';
import { plainClass } from '@nest-utils';
import { Body, Controller, ForbiddenException, Inject, Post, UseGuards } from '@nestjs/common';
import { ActivatedLicenseRepoInterface, LicenseRepoInterface } from '@repositories';
import { LICENSE_STATES } from '@utils';
import { In } from 'typeorm';

@Controller('check-license')
@UseGuards(CheckLicensePassKeyGuard)
export class CheckLicenseController {
  constructor(
    @Inject('REPOSITORIES.LICENSE')
    private readonly repo: LicenseRepoInterface,
    @Inject('REPOSITORIES.ACTIVATEDLICENSE')
    private readonly activatedLicenseRepo: ActivatedLicenseRepoInterface
  ) {}

  @Post()
  async check(@Body() inputs: BotLicenseSubscribeDtos.CheckLicenseDto) {
    const license = await this.repo.findOneOrFail({
      where: {
        licenseKey: inputs.licenseKey,
      },
      relations: {
        activatedLicenses: true,
      },
    });

    if (license.state == LICENSE_STATES.EXPIRED) {
      throw new ForbiddenException(`The license "${inputs.licenseKey}" has expired.`);
    } else if (license.state == LICENSE_STATES.CANCELED) {
      throw new ForbiddenException(`The license "${inputs.licenseKey}" has canceled.`);
    } else if (license.state == LICENSE_STATES.REVOKED) {
      throw new ForbiddenException(`The license "${inputs.licenseKey}" has revoked.`);
    }

    if (license.licenseDays >= 0 && license?.expiredAt && Date.now() > license?.expiredAt?.getTime()) {
      await this.repo.update({ licenseKey: inputs.licenseKey }, { state: LICENSE_STATES.EXPIRED });
      throw new ForbiddenException(`The license "${inputs.licenseKey}" has expired.`);
    }

    const isAccountActivated = !!license?.activatedLicenses.find(({ accountId }) => accountId == inputs?.accountId);

    if (isAccountActivated) {
      const output = await this.repo.mergeLicenseOptions(license, true);
      return {
        successfully: true,
        license: plainClass(LicenseDtos.LicenseDto, output),
      };
    }

    if (license?.accountNumber >= 0 && license?.activatedLicenses?.length >= license.accountNumber) {
      throw new ForbiddenException(`This license has reached account limit.`);
    }

    await this.activatedLicenseRepo.createOrFail({ ...inputs, licenseId: license.id });

    const newLicense = await this.repo.findOneBy({
      licenseKey: inputs.licenseKey,
      state: In([LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED]),
    });

    const output = await this.repo.mergeLicenseOptions(newLicense, true);
    return {
      successfully: true,
      license: plainClass(LicenseDtos.LicenseDto, output),
    };
  }
}
