module.exports = {
  apps: [
    {
      name: 'api',
      script: './dist/apps/api/main.js',
      env: {
        NODE_ENV: 'production',
        PORT: 8060,
        DB_MSQL_URL: 'mysql://root:rootPassword@alias-database-mysql:3306/ftmo_back_end?serverVersion=5.7',
        QUEUE_PREFIX: 'freemarket',
        LOGGER_USE_MONGO_DB: 'mongodb://ducph:password@alias-database-mongo:27017',
      },
      exec_mode: 'cluster',
    },
    {
      name: 'back-end',
      script: 'serve',
      env: {
        PM2_SERVE_PATH: './dist/apps/back-end',
        PM2_SERVE_PORT: 8061,
        PM2_SERVE_SPA: 'true',
      },
      exec_mode: 'cluster',
    },
    {
      name: 'front-end',
      script: 'serve',
      env: {
        PM2_SERVE_PATH: './dist/apps/front-end',
        PM2_SERVE_PORT: 8062,
        PM2_SERVE_SPA: 'true',
      },
      exec_mode: 'cluster',
    },
  ],
};
