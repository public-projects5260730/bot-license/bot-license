export const Error = ({ error }: { error: string | React.ReactElement }) => {
  return <div className="callout callout-danger text-danger p-2">{error}</div>;
};
