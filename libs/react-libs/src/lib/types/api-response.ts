export type IApiResponse<T = unknown> = T & {
  data?: T;
  error?: string;
  errors?: Record<string, string>;
};
