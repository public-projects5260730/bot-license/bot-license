import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Unique } from 'typeorm';

@Entity('agent_license_options')
@Unique(['agentId', 'type'])
export class AgentLicenseOption {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @Column()
  agentId: number;

  @Column({ length: 50 })
  type: string;

  @Column({ type: 'json' })
  options: Record<string, unknown>;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}
