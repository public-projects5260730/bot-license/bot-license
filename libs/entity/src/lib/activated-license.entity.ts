import { Entity, Column, PrimaryGeneratedColumn, UpdateDateColumn, Unique, ManyToOne, CreateDateColumn } from 'typeorm';
import { License } from './license.entity';

@Entity('activated_licenses')
@Unique(['licenseId', 'accountId'])
export class ActivatedLicense {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @Column()
  licenseId: number;

  @Column()
  accountId: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(() => License, (license) => license.activatedLicenses)
  License: License;
}
