import { LICENSE_STATES } from '@utils';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ActivatedLicense } from './activated-license.entity';
import { Agent } from './agent.entity';

@Entity('licenses')
export class License {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @Column()
  agentId: number;

  @Column({ length: 50, unique: true })
  licenseKey: string;

  @Column()
  accountNumber: number;

  @Column()
  licenseDays: number;

  @Column({ type: 'json', nullable: true })
  licenseOptions: Record<string, unknown> | null;

  @Column({ type: 'enum', enum: LICENSE_STATES, default: LICENSE_STATES.NEW })
  state: number;

  @Column()
  reason: string;

  @Column({ type: 'json', nullable: true })
  extra: Record<string, unknown> | null;

  @CreateDateColumn({ type: 'timestamp', nullable: true })
  activatedAt: Date;

  @CreateDateColumn({ type: 'timestamp', nullable: true })
  expiredAt: Date;

  @Column({ default: false })
  isTrial: boolean;

  @Column({ default: false })
  isSynced: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(() => Agent, (agent) => agent.liceneses)
  agent: Agent;

  @OneToMany(() => ActivatedLicense, (activatedLicense) => activatedLicense.License)
  activatedLicenses: ActivatedLicense[];
}
