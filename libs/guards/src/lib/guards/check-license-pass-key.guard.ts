import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { checkLicensePassKey } from '../configs';

@Injectable()
export class CheckLicensePassKeyGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    if (!checkLicensePassKey?.length) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const requestPassKey = request?.headers?.['x-pass-key'] || request?.body?.passKey || request?.query?.passKey;
    return checkLicensePassKey == requestPassKey;
  }
}
