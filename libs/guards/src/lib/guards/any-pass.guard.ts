import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class AnyPassGuard implements CanActivate {
  constructor(private readonly reflector?: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    if (!this?.reflector) {
      return true;
    }

    const anyPassGuards = this.reflector.get<any[]>('AnyPassGuards', context.getHandler());

    if (!anyPassGuards || anyPassGuards?.length === 0) {
      return true;
    }

    const guards = await Promise.all(
      anyPassGuards.map(async (guard) => {
        const instance = new guard();
        try {
          return !!(await instance?.canActivate(context));
        } catch (error) {
          return false;
        }
      })
    );

    return guards.some((result) => result === true);
  }
}
