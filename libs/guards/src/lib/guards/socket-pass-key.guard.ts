import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { socketPassKey } from '../configs';

@Injectable()
export class SocketPassKeyGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    if (!socketPassKey?.length) {
      return true;
    }
    const client = context.switchToWs().getClient();
    const data = context.switchToWs().getData();
    const clientPassKey =
      client?.handshake?.auth?.['x-pass-key'] ||
      client?.handshake?.headers?.['x-pass-key'] ||
      client?.handshake?.query?.passKey ||
      data?.passKey;

    if (clientPassKey !== socketPassKey) {
      client.emit('error', 'Invalid passkey');

      const ipAddress =
        client?.handshake?.headers['x-forwarded-for'] || client?.handshake?.address || client?.remoteAddress;
      console.log(`Invalid passkey IP: ${ipAddress} `);

      setTimeout(() => {
        if (client?.disconnect) {
          client.disconnect();
        } else if (client?.end) {
          client.end();
        } else if (client?.destroy) {
          client.destroy();
        }
      }, 200);

      return false;
    }
    return true;
  }
}
