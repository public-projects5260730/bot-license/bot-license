import { AgentRepoInterface } from '@repositories';
import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common';

@Injectable()
export class AgentSecretKeyGuard implements CanActivate {
  constructor(@Inject('REPOSITORIES.AGENT') private readonly repo: AgentRepoInterface) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const secretKey =
      request?.headers?.['x-agent-secret-key'] || request?.body?.agentSecretKey || request?.query?.agentSecretKey;

    if (!secretKey?.length) {
      return false;
    }

    const agent = await this.repo.findOneBy({ secretKey });
    if (!agent) {
      return false;
    }

    request.agent = agent;
    return true;
  }
}
