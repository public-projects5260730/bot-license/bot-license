export enum LICENSE_STATES {
  NEW = 1,
  ACTIVATED = 2,
  EXPIRED = 3,
  CANCELED = 4,
  REVOKED = 5,
}
