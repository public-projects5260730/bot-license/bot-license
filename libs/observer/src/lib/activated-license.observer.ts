import { Logger } from '@nestjs/common';
import { EntitySubscriberInterface, EventSubscriber, In, InsertEvent } from 'typeorm';
import { ActivatedLicense, License } from '@entity';
import { LICENSE_STATES } from '@utils';

@EventSubscriber()
export class ActivatedLicenseObserver implements EntitySubscriberInterface<ActivatedLicense> {
  listenTo() {
    return ActivatedLicense;
  }

  async afterInsert(event: InsertEvent<ActivatedLicense>) {
    const licenseRepo = event.manager.getRepository(License);
    const activeLicense = event.entity;

    const license = await licenseRepo.findOneBy({
      id: activeLicense.licenseId,
      state: In([LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED]),
    });

    if (!license) {
      return;
    }

    if (!license.activatedAt) {
      license.activatedAt = new Date();
    }

    await licenseRepo.update(license.id, license).catch(({ message }) => {
      Logger.log(message, { license, activeLicense });
    });
  }
}
