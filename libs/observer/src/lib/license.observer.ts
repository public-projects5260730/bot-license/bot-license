import { EntitySubscriberInterface, EventSubscriber, UpdateEvent, InsertEvent } from 'typeorm';
import { License } from '@entity';
import { LICENSE_STATES, addDays, date } from '@utils';

@EventSubscriber()
export class LicenseObserver implements EntitySubscriberInterface<License> {
  private orgEntity: License;

  listenTo() {
    return License;
  }

  afterLoad(entity: License) {
    this.orgEntity = entity;
  }

  beforeInsert(event: InsertEvent<License>) {
    if (event?.entity?.state == LICENSE_STATES.ACTIVATED) {
      event.entity.activatedAt = new Date();

      if (event?.entity?.licenseDays >= 0) {
        event.entity.expiredAt = addDays(date(event.entity.activatedAt), event.entity.licenseDays);
      }
    }
  }

  beforeUpdate(event: UpdateEvent<License>) {
    const { entity } = event;
    const orgEntity = this.orgEntity;

    if (entity?.state == LICENSE_STATES.ACTIVATED && !orgEntity?.activatedAt) {
      entity.activatedAt = new Date();
      event.entity = entity;
    }

    if (entity?.activatedAt || entity?.licensDays) {
      const licenseDays = entity?.licenseDays ?? orgEntity?.licenseDays;
      if (licenseDays < 0) {
        entity.expiredAt = null;
      } else if (entity?.activatedAt || orgEntity?.activatedAt) {
        entity.expiredAt = addDays(date(entity?.activatedAt || orgEntity.activatedAt), licenseDays);
      }

      event.entity = entity;
    }

    if ((entity?.activatedAt || orgEntity?.activatedAt) && (entity?.state || orgEntity.state) == LICENSE_STATES.NEW) {
      entity.state = LICENSE_STATES.ACTIVATED;
      event.entity = entity;
    } else if (
      [LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED].includes(entity?.state || orgEntity?.state) &&
      (entity?.expiredAt || orgEntity?.expiredAt) &&
      (entity?.expiredAt || orgEntity?.expiredAt) <= new Date()
    ) {
      entity.state = LICENSE_STATES.EXPIRED;
      event.entity = entity;
    }
  }
}
