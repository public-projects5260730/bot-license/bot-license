import { AgentLicenseOption } from '@entity';
import { BasicRepoInterface } from './basic.repo.interface';

export interface AgentLicenseOptionRepoInterface extends BasicRepoInterface<AgentLicenseOption> {}
