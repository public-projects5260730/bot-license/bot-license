import { ActivatedLicense } from '@entity';
import { BasicRepoInterface } from './basic.repo.interface';

export interface ActivatedLicenseRepoInterface extends BasicRepoInterface<ActivatedLicense> {}
