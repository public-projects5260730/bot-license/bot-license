import { Agent } from '@entity';
import { BasicRepoInterface } from './basic.repo.interface';
import { FindOptionsWhere } from 'typeorm';
import { AgentDtos } from '@dtos';

export interface AgentRepoInterface extends BasicRepoInterface<Agent> {
  generateSecretKey(): Promise<string>;
  findSearchWhere(filters: AgentDtos.SearchAgentDto): FindOptionsWhere<Agent>;
}
