import { ActivatedLicense, License } from '@entity';
import { BasicRepoInterface } from './basic.repo.interface';
import { LicenseDtos } from '@dtos';
import { FindOptionsWhere } from 'typeorm';

export interface LicenseRepoInterface extends BasicRepoInterface<License> {
  generateLicenseKey(): Promise<string>;
  updateLicenseByActiveLicense(activeLicense: ActivatedLicense): Promise<void>;
  mergeLicenseOptions(license: License, replace?: boolean): Promise<License & { botOptions?: Record<string, unknown> }>;
  findStatistics(): Promise<LicenseDtos.LicenseStatisticsDto[]>;
  findSearchWhere(filters: LicenseDtos.SearchLicenseDto): FindOptionsWhere<License>;
}
