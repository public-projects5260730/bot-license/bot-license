import { AgentLicenseOptionRepoInterface } from '@repositories';
import { BasicRepository } from './basic.repository';
import { AgentLicenseOption } from '@entity';
import { DataSource } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AgentLicenseOptionRepository
  extends BasicRepository<AgentLicenseOption>
  implements AgentLicenseOptionRepoInterface
{
  constructor(dataSource: DataSource) {
    super(AgentLicenseOption, dataSource.createEntityManager());
  }
}
