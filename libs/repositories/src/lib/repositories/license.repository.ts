import { AgentLicenseOptionRepoInterface, LicenseRepoInterface } from '@repositories';
import { BasicRepository } from './basic.repository';
import { ActivatedLicense, License } from '@entity';
import { Between, DataSource, FindOptionsWhere, In, LessThanOrEqual, Like, MoreThanOrEqual } from 'typeorm';
import { Inject, Injectable } from '@nestjs/common';
import { AGENT_OPTION_TYPES, LICENSE_STATES, arrayUnique, date, strRand } from '@utils';
import { LoggerService } from '@logger';
import { LicenseDtos } from '@dtos';
import { plainClass } from '@nest-utils';

@Injectable()
export class LicenseRepository extends BasicRepository<License> implements LicenseRepoInterface {
  @Inject('REPOSITORIES.AGENTLICENSEOPTION')
  private readonly agentOptionRepo: AgentLicenseOptionRepoInterface;

  @Inject(LoggerService)
  private readonly loggerService: LoggerService;

  constructor(dataSource: DataSource) {
    super(License, dataSource.createEntityManager());
  }

  generateLicenseKey = async (): Promise<string> => {
    let licenseKey: string;
    do {
      licenseKey = Array.apply(null, Array(5))
        .map(() => strRand(5))
        .join('-');
    } while (await this.countBy({ licenseKey }));

    return licenseKey;
  };

  updateLicenseByActiveLicense = async (activeLicense: ActivatedLicense) => {
    const license = await this.findOneBy({
      id: activeLicense.licenseId,
      state: In([LICENSE_STATES.NEW, LICENSE_STATES.ACTIVATED]),
    });

    if (!license) {
      return;
    }

    if (!license.activatedAt) {
      license.activatedAt = new Date();
    }

    await this.update(license.id, license).catch(({ message }) => {
      this.loggerService.setContext('licenses');
      this.loggerService.error(message, { license, activeLicense });
    });
  };

  findSearchWhere = (filters: LicenseDtos.SearchLicenseDto): FindOptionsWhere<License> => {
    let where: FindOptionsWhere<License> = {};

    if (filters?.licenseKey?.length) {
      where.licenseKey = Like(`%${filters.licenseKey}%`);
    }

    if (filters?.agentId) {
      where.agentId = filters.agentId;
    }

    if (filters?.state) {
      where.state = filters.state;
    }

    if (filters?.isTrial !== undefined) {
      where.isTrial = !!filters.isTrial;
    }

    if (filters?.activatedFromDate && filters?.activatedToDate) {
      where.activatedAt = Between(date(filters.activatedFromDate), date(filters.activatedToDate));
    } else if (filters?.activatedFromDate) {
      where.activatedAt = MoreThanOrEqual(date(filters.activatedFromDate));
    } else if (filters?.activatedToDate) {
      where.activatedAt = LessThanOrEqual(date(filters.activatedToDate));
    }

    if (filters?.expiredFromDate && filters?.expiredToDate) {
      where.expiredAt = Between(date(filters.expiredFromDate), date(filters.expiredToDate));
    } else if (filters?.expiredFromDate) {
      where.expiredAt = MoreThanOrEqual(date(filters.expiredFromDate));
    } else if (filters?.expiredToDate) {
      where.expiredAt = LessThanOrEqual(date(filters.expiredToDate));
    }

    if (filters?.fromDate && filters?.toDate) {
      where.createdAt = Between(date(filters.fromDate), date(filters.toDate));
    } else if (filters?.fromDate) {
      where.createdAt = MoreThanOrEqual(date(filters.fromDate));
    } else if (filters?.toDate) {
      where.createdAt = LessThanOrEqual(date(filters.toDate));
    }

    return where;
  };

  findStatistics = async (): Promise<LicenseDtos.LicenseStatisticsDto[]> => {
    const result = await this.createQueryBuilder()
      .select('state')
      .addSelect('COUNT(id)', 'counter')
      .groupBy('state')
      .orderBy('state', 'ASC')
      .getRawMany();

    return plainClass(LicenseDtos.LicenseStatisticsDto, result);
  };

  mergeLicenseOptions = async (
    license: License,
    replace?: boolean
  ): Promise<License & { botOptions?: Record<string, unknown> }> => {
    const { licenseOptions } = license;

    const output = replace ? license : { ...license, botOptions: licenseOptions };

    if (!license?.agentId || !licenseOptions || !Object.keys(licenseOptions)?.length) {
      return output;
    }

    const types = this.getAgentOptionTypes(license);
    if (!types) {
      return output;
    }

    const agentOptions = await this.agentOptionRepo.findBy({ agentId: license.agentId, type: In(types) });
    if (!agentOptions?.length) {
      return output;
    }

    const options = types.reduceRight((options, type) => {
      const agentOption = agentOptions.find((agentOption) => agentOption.type == type);
      if (!agentOption?.options) {
        return options;
      }
      return { ...options, ...agentOption.options };
    }, licenseOptions || {});

    if (replace) {
      output.licenseOptions = options;
      return output;
    }
    return { ...output, botOptions: options };
  };

  private getAgentOptionTypes = (license: License): string[] => {
    const { licenseOptions } = license;
    if (!licenseOptions) {
      return [];
    }

    const types: any = licenseOptions?.[AGENT_OPTION_TYPES];
    if (!types || !types?.length) {
      return [];
    }
    const arrTypes = Array.isArray(types) ? types : [types];

    return arrayUnique(arrTypes.filter((type) => typeof type === 'string'));
  };
}
