import { ActivatedLicenseRepoInterface } from '@repositories';
import { BasicRepository } from './basic.repository';
import { ActivatedLicense } from '@entity';
import { DataSource, In } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ActivatedLicenseRepository
  extends BasicRepository<ActivatedLicense>
  implements ActivatedLicenseRepoInterface
{
  constructor(dataSource: DataSource) {
    super(ActivatedLicense, dataSource.createEntityManager());
  }
}
