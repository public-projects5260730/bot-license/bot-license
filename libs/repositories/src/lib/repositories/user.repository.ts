import { UserRepoInterface } from '@repositories';
import { BasicRepository } from './basic.repository';
import { User } from '@entity';
import { DataSource, DeepPartial } from 'typeorm';
import { BadRequestException, Injectable } from '@nestjs/common';

@Injectable()
export class UserRepository extends BasicRepository<User> implements UserRepoInterface {
  constructor(dataSource: DataSource) {
    super(User, dataSource.createEntityManager());
  }

  register = (entityLike: DeepPartial<User>): Promise<User> => {
    const entity = this.create(entityLike);
    return this.save(entity).catch(({ errno, message, ...args }) => {
      if (errno != 1062) {
        throw new BadRequestException({ message });
      }
      const errors = {
        email: 'Email already exists',
      };
      throw new BadRequestException({ errors });
    });
  };
}
