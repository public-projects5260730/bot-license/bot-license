import { AgentRepoInterface } from '@repositories';
import { BasicRepository } from './basic.repository';
import { Agent } from '@entity';
import { Between, DataSource, FindOptionsWhere, LessThanOrEqual, Like, MoreThanOrEqual } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { date, strRand } from '@utils';
import { AgentDtos } from '@dtos';

@Injectable()
export class AgentRepository extends BasicRepository<Agent> implements AgentRepoInterface {
  constructor(dataSource: DataSource) {
    super(Agent, dataSource.createEntityManager());
  }

  generateSecretKey = async (): Promise<string> => {
    let secretKey: string;
    do {
      secretKey = strRand(36);
    } while (await this.countBy({ secretKey }));

    return secretKey;
  };

  findSearchWhere = (filters: AgentDtos.SearchAgentDto): FindOptionsWhere<Agent> => {
    let where: FindOptionsWhere<Agent> = {};
    if (filters?.name?.length) {
      where.name = Like(`%${filters.name}%`);
    }
    if (filters?.fromDate && filters?.toDate) {
      where.createdAt = Between(date(filters.fromDate), date(filters.toDate));
    } else if (filters?.fromDate) {
      where.createdAt = MoreThanOrEqual(date(filters.fromDate));
    } else if (filters?.toDate) {
      where.createdAt = LessThanOrEqual(date(filters.toDate));
    }

    return where;
  };
}
