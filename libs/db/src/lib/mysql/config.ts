import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

const config: TypeOrmModuleOptions = {
  type: 'mysql',
  namingStrategy: new SnakeNamingStrategy(),
  host: process?.env?.DB_MSQL_HOST || 'localhost',
  port: +(process?.env?.DB_MSQL_POST || 3306),
  username: process?.env?.DB_MSQL_USERNAME || 'root',
  password: process?.env?.DB_MSQL_PASSWORD || '',
  database: process?.env?.DB_MSQL_DATABASE || 'demo',
  autoLoadEntities: true,
};

export default config;
