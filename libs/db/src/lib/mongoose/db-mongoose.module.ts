import { DynamicModule, Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleFactoryOptions } from '@nestjs/mongoose';
import config from './config';

@Module({})
export class DBMongooseModule {
  static forRoot(options?: MongooseModuleFactoryOptions): DynamicModule {
    const uri = options?.uri || config?.uri;

    return {
      module: DBMongooseModule,
      imports: [
        MongooseModule.forRoot(uri, {
          ...config,
          ...options,
          uri: undefined,
        }),
      ],
    };
  }
}
