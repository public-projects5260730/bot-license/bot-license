import { MongooseModuleOptions } from '@nestjs/mongoose';

const config: MongooseModuleOptions = {
  uri: process?.env?.DB_MONGO_URL || '',
  dbName: process?.env?.DB_MONGO_DB || '',
};

export default config;
