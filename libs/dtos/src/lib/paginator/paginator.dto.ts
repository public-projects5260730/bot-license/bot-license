export class PaginatorDto<T> {
  page: number;
  perPage: number;
  total: number;
  resultCount: number;
  items: T[];
}
