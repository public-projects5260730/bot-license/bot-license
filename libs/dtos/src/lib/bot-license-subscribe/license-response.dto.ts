import { LicenseDtos } from '@dtos';
import { HttpStatus } from '@nestjs/common';
import { Expose, Transform, Type } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsOptional } from 'class-validator';

export class LicenseResponseDto {
  @Expose()
  @IsOptional()
  @Transform(({ value }) => +value || HttpStatus.OK)
  status: number;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => !!value)
  @IsBoolean()
  successfully: boolean;

  @Expose()
  @IsNotEmpty()
  event: string;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => value || '')
  error: string;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => value || {})
  errors: Record<string, string>;

  @Expose()
  @IsOptional()
  @Transform(({ value }) => value || {})
  @Type(() => LicenseDtos.LicenseDto)
  license: LicenseDtos.LicenseDto;
}
