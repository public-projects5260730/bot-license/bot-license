import { Expose, Transform } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CheckLicenseDto {
  @Expose()
  @Transform(({ value }) => +value)
  @IsNumber()
  @IsNotEmpty()
  accountId: number;

  @Expose()
  @IsString()
  @IsNotEmpty()
  licenseKey: string;
}
