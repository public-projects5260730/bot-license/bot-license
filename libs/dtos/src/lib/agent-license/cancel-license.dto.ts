import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CancelLicenseDto {
  @IsNotEmpty()
  @IsString()
  licenseKey: string;

  @IsOptional()
  reason: string;
}
