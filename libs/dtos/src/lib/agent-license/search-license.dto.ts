import { IsNotEmpty } from 'class-validator';

export class SearchLicenseList {
  @IsNotEmpty()
  licenseKeys: string[];
}
