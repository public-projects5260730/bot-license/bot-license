import { PartialType } from '@nestjs/mapped-types';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { CreateLicenseDto } from './create-license.dto';

export class EditLicenseDto extends PartialType(CreateLicenseDto) {
  @IsNotEmpty()
  @IsString()
  licenseKey: string;

  @IsOptional()
  reason: string;
}
