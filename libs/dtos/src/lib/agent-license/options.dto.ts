import { jsonParse } from '@utils';
import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

export class OptionsDto {
  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @Transform(({ value }) => jsonParse(value) || {})
  options: Record<string, unknown>;
}
