import { PartialType } from '@nestjs/mapped-types';
import { CreateLicenseDto } from './create-license.dto';
import { IsOptional } from 'class-validator';

export class EditLicenseDto extends PartialType(CreateLicenseDto) {
  @IsOptional()
  reason?: string;
}
