import { timestamp } from '@utils';
import { Expose, Transform } from 'class-transformer';

export class ActivatedLicenseDto {
  @Expose()
  id: number;

  @Expose()
  accountId: number;

  @Expose()
  licenseId: number;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  createdAt: number;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  updatedAt: number;
}
