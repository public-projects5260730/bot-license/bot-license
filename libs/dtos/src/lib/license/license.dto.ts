import { AgentDtos } from '@dtos';
import { LICENSE_STATES, jsonParse, timestamp } from '@utils';
import { Expose, Transform, Type } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { ActivatedLicenseDto } from './activated-license.dto';

export class LicenseDto {
  @Expose()
  id: number;

  @Expose()
  licenseKey: string;

  @Expose()
  accountNumber: number;

  @Expose()
  licenseDays: number;

  @Expose()
  @Transform(({ value }) => jsonParse(value) || {})
  licenseOptions: Record<string, unknown>;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  activatedAt: number;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  expiredAt: number;

  @Expose()
  state: LICENSE_STATES;

  @Expose()
  reason: string;

  @Expose()
  isTrial: boolean;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  createdAt: number;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  updatedAt: number;

  @Expose()
  @IsOptional()
  @Transform(({ obj }) => obj?.activatedLicenses?.map(({ accountId }: { accountId: string }) => +accountId))
  activatedAccountIds?: number[];

  @Expose()
  @IsOptional()
  @Type(() => AgentDtos.AgentDto)
  agent: AgentDtos.AgentDto;

  @IsOptional()
  @Transform(({ value }) => jsonParse(value) || {})
  botOptions: Record<string, unknown>;

  @IsOptional()
  @Type(() => ActivatedLicenseDto)
  activatedLicenses: ActivatedLicenseDto[];
}
