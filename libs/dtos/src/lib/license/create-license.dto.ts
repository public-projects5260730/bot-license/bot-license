import { LICENSE_STATES, jsonParse } from '@utils';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsInt, IsNotEmpty, IsOptional, Min, NotEquals } from 'class-validator';

export class CreateLicenseDto {
  @IsNotEmpty()
  @Transform(({ value }) => +value)
  @IsInt()
  agentId: number;

  @IsNotEmpty()
  @Transform(({ value }) => +value)
  @IsInt()
  @Min(-1)
  @NotEquals(0)
  accountNumber: number;

  @IsNotEmpty()
  @Transform(({ value }) => +value)
  @IsInt()
  @Min(-1)
  @NotEquals(0)
  licenseDays: number;

  @IsOptional()
  @Transform(({ value }) => (value ? jsonParse(value) : {}))
  licenseOptions: Record<string, unknown>;

  @IsOptional()
  @Transform(({ value }) => +value || LICENSE_STATES.NEW)
  @IsEnum(LICENSE_STATES)
  state: LICENSE_STATES;

  @IsOptional()
  @Transform(({ value }) => !!value)
  @IsBoolean()
  isTrial: boolean;
}
