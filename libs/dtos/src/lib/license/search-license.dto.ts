import { PartialType } from '@nestjs/mapped-types';
import { LICENSE_STATES, date } from '@utils';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsOptional } from 'class-validator';
import { PaginateDto } from '../paginator';

export class SearchLicenseDto extends PartialType(PaginateDto) {
  @IsOptional()
  licenseKey?: string;

  @IsOptional()
  agentId?: number;

  @IsOptional()
  @Transform(({ value }) => +value)
  @IsEnum(LICENSE_STATES)
  state?: LICENSE_STATES;

  @IsOptional()
  @Transform(({ value }) => (isNaN(+value) ? !!value : !!+value))
  @IsBoolean()
  isTrial?: boolean;

  @IsOptional()
  @Transform(({ value }) => date(value))
  activatedFromDate?: number | Date;

  @IsOptional()
  @Transform(({ value }) => date(value))
  activatedToDate?: number | Date;

  @IsOptional()
  @Transform(({ value }) => date(value))
  expiredFromDate?: number | Date;

  @IsOptional()
  @Transform(({ value }) => date(value))
  expiredToDate?: number | Date;

  @IsOptional()
  @Transform(({ value }) => date(value))
  fromDate?: number | Date;

  @IsOptional()
  @Transform(({ value }) => date(value))
  toDate?: number | Date;
}
