import { AgentDtos } from '@dtos';
import { LICENSE_STATES } from '@utils';
import { Expose } from 'class-transformer';

export class LicenseStatisticsDto {
  @Expose()
  state: LICENSE_STATES;

  @Expose()
  counter: number;
}
