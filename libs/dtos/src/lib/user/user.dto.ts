import { timestamp } from '@utils';
import { Expose, Transform } from 'class-transformer';

export class UserDto {
  @Expose()
  id: number;

  @Expose()
  name: string;

  @Expose()
  email: string;

  @Expose()
  @Transform(({ value }) => timestamp(value))
  createdAt: number;

  @Expose()
  @Transform(({ value }) => timestamp(value))
  updatedAt: number;
}
