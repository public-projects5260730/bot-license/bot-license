import { PartialType } from '@nestjs/mapped-types';
import { date } from '@utils';
import { Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { PaginateDto } from '../paginator';

export class SearchAgentDto extends PartialType(PaginateDto) {
  @IsOptional()
  @IsOptional()
  name?: string;

  @IsOptional()
  @Transform(({ value }) => date(value))
  fromDate?: number | Date;

  @IsOptional()
  @Transform(({ value }) => date(value))
  toDate?: number | Date;
}
