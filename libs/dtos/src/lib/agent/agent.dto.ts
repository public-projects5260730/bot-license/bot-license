import { LicenseDtos } from '@dtos';
import { timestamp } from '@utils';
import { Expose, Transform, Type } from 'class-transformer';
import { IsOptional } from 'class-validator';

export class AgentDto {
  @Expose()
  id: number;

  @Expose()
  name: string;

  @Expose()
  secretKey: string;

  @Expose()
  webHook: string;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  createdAt: number;

  @Expose()
  @Transform(({ value }) => (value ? timestamp(value) : null))
  updatedAt: number;

  @Expose()
  @IsOptional()
  @Type(() => LicenseDtos.LicenseDto)
  licenses: LicenseDtos.LicenseDto[];
}
