import { Transform } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsUrl } from 'class-validator';

export class CreateAgentDto {
  @IsNotEmpty()
  name: string;

  @IsOptional()
  @Transform(({ value }) => value || undefined)
  @IsUrl()
  webHook: string;
}
