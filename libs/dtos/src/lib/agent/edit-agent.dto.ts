import { IsBoolean, IsOptional } from 'class-validator';
import { CreateAgentDto } from './create-agent.dto';
import { Transform } from 'class-transformer';
import { PartialType } from '@nestjs/mapped-types';

export class EditAgentDto extends PartialType(CreateAgentDto) {
  @IsOptional()
  @Transform(({ value }) => !!value)
  @IsBoolean()
  isChangeSecretKey: boolean;
}
